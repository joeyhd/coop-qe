
#include "rm.h"
#include <cstring>

RelationManager* RelationManager::instance()
{
    static RelationManager _rm;
    return &_rm;
}

RelationManager::RelationManager():tableFileName("Tables"), columnFileName("Columns")
{
    this->_tableNum = 0;
    this->_rbfm = RecordBasedFileManager::instance();
    this->_ixm = IndexManager::instance();
    this->initTableDescriptor();
    this->initColumnDescriptor();
    // load if Tables and Columns exist,
    this->initOpen();
}

void RelationManager::initOpen()
{
    if(utils::FileExists(this->tableFileName))
    {
        this->openFile(this->tableFileName, &this->tblFileHandle);
        RM_ScanIterator rmscan;
        vector<string> attrs;
        attrs.push_back("table-id");
        this->scan("Tables", "", NO_OP, NULL, attrs, rmscan);
        char data[PAGE_SIZE]; RID rid;
        while(rmscan.getNextTuple(rid, data) != RM_EOF)
            this->_tableNum = *(int*)(data+1) + 1;
    }
    if(utils::FileExists(this->columnFileName))
        this->openFile(this->columnFileName, &this->colFileHandle);
}

RelationManager::~RelationManager()
{
    this->closeAllFiles();
}

RC RelationManager::createCatalog()
{
    RC rc;
    if(utils::FileExists(this->tableFileName) || utils::FileExists(this->columnFileName))
        return FILE_EXISTS;
    rc = this->_rbfm->createFile(this->tableFileName); if(rc != STD_SUCC) return rc;
    rc = this->_rbfm->createFile(this->columnFileName); if(rc != STD_SUCC) return rc;
    rc = this->initTableFile(); if(rc != STD_SUCC) return rc;
    rc = this->initColumnFile(); if(rc != STD_SUCC) return rc;
    return STD_SUCC;
}

void RelationManager::initTableDescriptor()
{
    this->tableDescriptor.clear();
    this->tableDescriptor.push_back(utils::setAttribute("table-id", TypeInt, 4));
    this->tableDescriptor.push_back(utils::setAttribute("table-name", TypeVarChar, 50));
    this->tableDescriptor.push_back(utils::setAttribute("file-name", TypeVarChar, 50));
}

void RelationManager::initColumnDescriptor()
{
    this->columnDescriptor.clear();
    this->columnDescriptor.push_back(utils::setAttribute("table-id", TypeInt, 4));
    this->columnDescriptor.push_back(utils::setAttribute("column-name", TypeVarChar, 50));
    this->columnDescriptor.push_back(utils::setAttribute("column-type", TypeInt, 4));
    this->columnDescriptor.push_back(utils::setAttribute("column-length", TypeInt, 4));
    this->columnDescriptor.push_back(utils::setAttribute("column-position", TypeInt, 4));
}

RC RelationManager::initTableFile()
{
    RID rid;
    // open file
    RC rc = this->openFile(this->tableFileName, &this->tblFileHandle);
    if(rc != STD_SUCC) return rc;
    // write records
    this->_dg.setInserter(this->_rbfm, this->tblFileHandle, this->tableDescriptor);
    this->_dg << 1 << "Tables" << this->tableFileName << INSD;
    this->_dg << 2 << "Columns" << this->columnFileName << INSD;
    this->_tableNum = 2;
    return STD_SUCC;
}

RC RelationManager::initColumnFile()
{
    RID rid;
    // open file
    RC rc = this->openFile(this->columnFileName, &this->colFileHandle);
    if(rc != STD_SUCC) return rc;
    // write records
    this->_dg.setInserter(this->_rbfm, this->colFileHandle, this->columnDescriptor);
    this->_dg << 1 << "table-id" << TypeInt << 4 << 1 << INSD;
    this->_dg << 1 << "table-name" << TypeVarChar << 50 << 2 << INSD;
    this->_dg << 1 << "file-name" << TypeVarChar << 50  << 3 << INSD;
    this->_dg << 2 << "table-id" << TypeInt << 4 << 1 << INSD;
    this->_dg << 2 << "column-name" << TypeVarChar << 50 << 2 << INSD;
    this->_dg << 2 << "column-type" << TypeInt << 4 << 3 << INSD;
    this->_dg << 2 << "column-length" << TypeInt << 4 << 4 << INSD;
    this->_dg << 2 << "column-position" << TypeInt << 4 << 5 << INSD;
    return STD_SUCC;
}

RC RelationManager::deleteCatalog()
{
    this->closeFile(this->tableFileName); this->closeFile(this->columnFileName);
    if(utils::FileExists(this->tableFileName)) remove(this->tableFileName.c_str());
    if(utils::FileExists(this->columnFileName)) remove(this->columnFileName.c_str());
    return STD_SUCC;
}

RC RelationManager::createTable(const string &tableName, const vector<Attribute> &attrs)
{
    RC rc = this->_rbfm->createFile(tableName); if (rc != STD_SUCC) return rc;
    // insert tuple into table.
    this->_dg.setInserter(this->_rbfm, this->tblFileHandle, this->tableDescriptor);
    this->_dg << ++this->_tableNum << tableName << tableName << INSD;
    // insert tuple into columns
    this->_dg.setInserter(this->_rbfm, this->colFileHandle, this->columnDescriptor);
    for (int i = 0; i < attrs.size(); i++)
        this->_dg << this->_tableNum << attrs[i].name << attrs[i].type << attrs[i].length << i+1 << INSD;
    return STD_SUCC;
}

RC RelationManager::deleteTable(const string &tableName)
{
    if(tableName == this->tableFileName || tableName == this->columnFileName) return PERMISSION_DENIED;
    if(utils::FileExists(tableName)) remove(tableName.c_str());
    // delete record in Tables
    RM_ScanIterator rmscan;
    vector<string> t_attr; t_attr.push_back("table-id");
    char cmpdata[PAGE_SIZE] = {};
    *((int*)cmpdata) = tableName.length();
    memcpy(cmpdata+4, tableName.c_str(), tableName.length());
    this->scan("Tables", "table-name", EQ_OP, cmpdata, t_attr, rmscan);
    RID rid; char data[PAGE_SIZE] = {};
    RC rc = rmscan.getNextTuple(rid, data); if(rc != STD_SUCC) return rc;
    this->_rbfm->deleteRecord(*this->tblFileHandle, this->tableDescriptor, rid);
    // delete record in Columns
    t_attr.clear();
    this->scan("Columns", "table-id", EQ_OP, data+1, t_attr, rmscan);
    char data2[PAGE_SIZE];
    while(rmscan.getNextTuple(rid, data2) != RM_EOF)
    {
        this->_rbfm->deleteRecord(*this->colFileHandle, this->columnDescriptor, rid);
        memset(data2, 0, PAGE_SIZE);
    }
    return STD_SUCC;
}

RC RelationManager::getAttributes(const string &tableName, vector<Attribute> &attrs)
{
    if(tableName == "Tables")
    {
        attrs = this->tableDescriptor;
        return STD_SUCC;
    }
    if(tableName == "Columns")
    {
        attrs = this->columnDescriptor;
        return STD_SUCC;
    }
    RM_ScanIterator rmscan;
    vector<string> t_attr; t_attr.push_back("table-id");
    char cmpdata[PAGE_SIZE] = {};
    *((int*)cmpdata) = tableName.length();
    memcpy(cmpdata+4, tableName.c_str(), tableName.length());
    this->scan("Tables", "table-name", EQ_OP, cmpdata, t_attr, rmscan);
    RID rid; char data[PAGE_SIZE] = {};
    RC rc = rmscan.getNextTuple(rid, data); if(rc != STD_SUCC) return rc;
    t_attr.clear();
    t_attr.push_back("column-name");
    t_attr.push_back("column-type");
    t_attr.push_back("column-length");
    this->scan("Columns", "table-id", EQ_OP, data+1, t_attr, rmscan);
    char data2[PAGE_SIZE]; attrs.clear();
    while(rmscan.getNextTuple(rid, data2) != RM_EOF)
    {
        attrs.push_back(utils::convertToAttribute(data2));
        memset(data2, 0, PAGE_SIZE);
    }
    return STD_SUCC;
}

RC RelationManager::insertTuple(const string &tableName, const void *data, RID &rid)
{
    if(tableName == "Columns" || tableName == "Tables") return NOT_AUTHORIZED;
    if(!utils::FileExists(tableName)) return FILE_NOT_EXISTS;
    FileHandle* fileHandle;
    vector<Attribute> recordDescriptor;
    this->getAttributes(tableName, recordDescriptor);
    this->openFile(tableName, &fileHandle);
    RC rc = this->_rbfm->insertRecord(*fileHandle, recordDescriptor, data, rid);
    if(rc == RM_EOF) this->closeFile(tableName);
    // insert to index files
    vector<Attribute>::const_iterator it;
    if (this->_ixFileHandle == NULL) this->_ixFileHandle = new IXFileHandle();
    for (it = recordDescriptor.begin(); it != recordDescriptor.end(); it++) {
        RC ixrc = this->_ixm->openFile(tableName + (*it).name, *this->_ixFileHandle);
        if (ixrc == STD_SUCC) {
            char key[PAGE_SIZE] = {};
            vector<string> attr;
            attr.push_back((*it).name);
            utils::extractAttrFromData(data, recordDescriptor, key, attr);
            this->_ixm->insertEntry(*this->_ixFileHandle, *it, key + 1, rid);
        }
        this->_ixm->closeFile(*this->_ixFileHandle);
    }
    return rc;
}

RC RelationManager::deleteTuple(const string &tableName, const RID &rid)
{
    if(tableName == "Columns" || tableName == "Tables") return NOT_AUTHORIZED;
    if(!utils::FileExists(tableName)) return FILE_NOT_EXISTS;
    FileHandle* fileHandle;
    vector<Attribute> recordDescriptor;
    this->getAttributes(tableName, recordDescriptor);
    this->openFile(tableName, &fileHandle);
    RC rc = this->_rbfm->deleteRecord(*fileHandle, recordDescriptor, rid);
    this->closeFile(tableName);
    return rc;
}

RC RelationManager::updateTuple(const string &tableName, const void *data, const RID &rid)
{
    if(tableName == "Columns" || tableName == "Tables") return NOT_AUTHORIZED;
    if(!utils::FileExists(tableName)) return FILE_NOT_EXISTS;
    FileHandle* fileHandle;
    vector<Attribute> recordDescriptor;
    this->getAttributes(tableName, recordDescriptor);
    this->openFile(tableName, &fileHandle);
    RC rc = this->_rbfm->updateRecord(*fileHandle, recordDescriptor, data, rid);
    this->closeFile(tableName);
    return rc;
}

RC RelationManager::readTuple(const string &tableName, const RID &rid, void *data)
{
    if(!utils::FileExists(tableName)) return FILE_NOT_EXISTS;
    FileHandle* fileHandle;
    vector<Attribute> recordDescriptor;
    this->getAttributes(tableName, recordDescriptor);
    this->openFile(tableName, &fileHandle);
    RC rc = this->_rbfm->readRecord(*fileHandle, recordDescriptor, rid, data);
    this->closeFile(tableName);
    return rc;
}

RC RelationManager::printTuple(const vector<Attribute> &attrs, const void *data)
{
    RC rc = this->_rbfm->printRecord(attrs, data);
	return rc;
}

RC RelationManager::readAttribute(const string &tableName, const RID &rid, const string &attributeName, void *data)
{
    if(!utils::FileExists(tableName)) return FILE_NOT_EXISTS;
    vector<Attribute> recordDescriptor;
    FileHandle* fileHandle;
    this->openFile(tableName, &fileHandle);
    this->getAttributes(tableName, recordDescriptor);
    RC rc = this->_rbfm->readAttribute(*fileHandle, recordDescriptor, rid, attributeName, data);
    this->closeFile(tableName);
    return rc;
}

RC RelationManager::scan(const string &tableName,
      const string &conditionAttribute,
      const CompOp compOp,
      const void *value,
      const vector<string> &attributeNames,
      RM_ScanIterator &rm_ScanIterator)
{
    vector<Attribute> descriptor;
    RC rc = this->getAttributes(tableName, descriptor);
    if(rc != STD_SUCC) return rc;
    rc = this->openFile(tableName, &(rm_ScanIterator.fileHandle()));
    if(rc != STD_SUCC) return rc;
    this->_rbfm->scan(*rm_ScanIterator.fileHandle(), descriptor, conditionAttribute,
                    compOp, value, attributeNames, rm_ScanIterator.scanIterator());
    rm_ScanIterator.setFileName(tableName);
    return STD_SUCC;
}

RC RelationManager::openFile(const string &fileName, FileHandle** fileHandle)
{
    map<string, FileHandle*>::iterator got = this->_fileHandleMap.find(fileName);
    map<string, unsigned>::iterator cntGot = this->_fileHandleOpenCnt.find(fileName);
    if(got != this->_fileHandleMap.end()) { 
        *fileHandle = got->second;
        cntGot->second ++;
    }
    else {
        FileHandle* newFileHandle = new FileHandle;
        RC rc = this->_rbfm->openFile(fileName, *newFileHandle);
        if(rc != STD_SUCC) return rc;
        this->_fileHandleMap.insert(pair<string, FileHandle*>(fileName, newFileHandle));
        this->_fileHandleOpenCnt.insert(pair<string, unsigned>(fileName, 1));
        *fileHandle = newFileHandle;
    }
    return STD_SUCC;
}

RC RelationManager::closeFile(const string &fileName)
{
    map<string, FileHandle*>::iterator got = this->_fileHandleMap.find(fileName);
    map<string, unsigned>::iterator cntGot = this->_fileHandleOpenCnt.find(fileName);
    if(got == this->_fileHandleMap.end()) return STD_ERR;
    if(cntGot->second > 1) {
        cntGot->second --;
        return STD_SUCC;
    }
    got->second->~FileHandle();
    this->_fileHandleMap.erase(got);
    this->_fileHandleOpenCnt.erase(cntGot);
    return STD_SUCC;
}

RC RelationManager::closeAllFiles()
{
    map<string, FileHandle*>::const_iterator got = this->_fileHandleMap.begin();
    while(got != this->_fileHandleMap.end()){
        got->second->~FileHandle();
        got ++;
    }
    this->_fileHandleMap.clear();
    this->_fileHandleOpenCnt.clear();
    return STD_SUCC;
}

// Extra credit work
RC RelationManager::dropAttribute(const string &tableName, const string &attributeName)
{
    return -1;
}

// Extra credit work
RC RelationManager::addAttribute(const string &tableName, const Attribute &attr)
{
    return -1;
}

RC RM_ScanIterator::getNextTuple(RID &rid, void *data)
{
    return this->_rbfm_scan.getNextRecord(rid, data);
}

void RM_ScanIterator::setFileName(const string &fileName)
{
    this->_fileName = fileName;
}

string& RM_ScanIterator::getFileName()
{
    return this->_fileName;
}

RC RM_ScanIterator::close()
{
    return this->_rbfm_scan.close();
}

RBFM_ScanIterator& RM_ScanIterator::scanIterator()
{
    return this->_rbfm_scan;
}

FileHandle*& RM_ScanIterator::fileHandle()
{
    return this->_fileHandle;
}

RM_IndexScanIterator::RM_IndexScanIterator() {
    _ixIt = new IX_ScanIterator();
    _ixFileHandle = new IXFileHandle();
}

RM_IndexScanIterator::~RM_IndexScanIterator() {
    this->_ixIt->~IX_ScanIterator();
}

RC RM_IndexScanIterator::getNextEntry(RID &rid, void *key) {
    return _ixIt->getNextEntry(rid, key);
}

RC RM_IndexScanIterator::close() {
    return _ixIt->close();
}

RC RelationManager::createIndex(const string &tableName, const string &attributeName)
{
    IXFileHandle ixfileHandle;
    IX_ScanIterator ix_ScanIterator;
    RC rc = this->_ixm->createFile(tableName + attributeName);
    if (rc != STD_SUCC) return rc;
    rc = this->_ixm->openFile(tableName + attributeName, ixfileHandle);
    if (rc != STD_SUCC) return rc;
    RM_ScanIterator rmsi;
    vector<string> attr;
    attr.push_back(attributeName);
    rc = this->scan(tableName, "", NO_OP, NULL, attr, rmsi);
    if (rc != STD_SUCC) return rc;
    vector<Attribute> attrs;
    this->getAttributes(tableName, attrs);
    vector<Attribute>::iterator it;
    Attribute attribute;
    for (it = attrs.begin(); it != attrs.end(); it++) {
        if ((*it).name == attributeName) {
            attribute = *it;
            break;
        }
    }
    RID rid;
    char buf[PAGE_SIZE];
    while(rmsi.getNextTuple(rid, buf) != RM_EOF) {
        this->_ixm->insertEntry(ixfileHandle, attribute, (void *)((char *)buf + 1), rid);
    }
    ix_ScanIterator.close();
    this->_ixm->closeFile(ixfileHandle);
    return STD_SUCC;
}

RC RelationManager::destroyIndex(const string &tableName, const string &attributeName)
{
    RC rc = _ixm->destroyFile(tableName + attributeName);
    return rc;
}

RC RelationManager::indexScan(const string &tableName,
                      const string &attributeName,
                      const void *lowKey,
                      const void *highKey,
                      bool lowKeyInclusive,
                      bool highKeyInclusive,
                      RM_IndexScanIterator &rm_IndexScanIterator)
{
    if (this->_ixFileHandle == NULL) this->_ixFileHandle = new IXFileHandle;
    string indexFileName = tableName + attributeName;
    // how to check if file already open?
    RC rc = _ixm->openFile(indexFileName, *rm_IndexScanIterator._ixFileHandle);
    if (rc != STD_SUCC) return rc;
    // find attribute according to attributeName
    vector<Attribute> attrs;
    this->getAttributes(tableName, attrs);
    vector<Attribute>::iterator it;
    Attribute attr;
    for (it = attrs.begin(); it != attrs.end(); it++) {
        if ((*it).name == attributeName) {
            attr = *it;
            break;
        }
    }
    rc = this->_ixm->scan(*rm_IndexScanIterator._ixFileHandle, attr, lowKey, highKey, lowKeyInclusive, highKeyInclusive, *rm_IndexScanIterator._ixIt);
    return rc;
}
