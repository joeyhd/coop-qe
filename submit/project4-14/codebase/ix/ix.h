#ifndef _ix_h_
#define _ix_h_

#include <vector>
#include <string>

#include "../rbf/pfm.h"
#include "../rbf/rbfm.h"

# define IX_EOF (-1)  // end of the index scan
# define MAX_SLOT 40 // !!!! varchar case

class IndexNode{
public:
    IndexNode(){this->page = new char[PAGE_SIZE] {};}
    ~IndexNode(){delete[] this->page;}
    unsigned short freeSpace;
    unsigned short numOfSlots;
    unsigned pageNum;
    vector<void*> keys;
    vector<unsigned> children;
    char* page;
    void operator=(const IndexNode& b);
};

class LeafNode{
public:
    LeafNode(){this->page = new char[PAGE_SIZE] {};}
    ~LeafNode(){delete[] this->page;}
    unsigned short freeSpace;
    unsigned short numOfSlots;
    unsigned pageNum;
    unsigned next;
    vector< pair<void*, RID> > entries; // <key, rid>
    char* page;
    void operator=(const LeafNode& b);
};

class IX_ScanIterator;
class IXFileHandle;

class IndexManager {

public:
    static IndexManager* instance();

    // Create an index file.
    RC createFile(const string &fileName);

    // Delete an index file.
    RC destroyFile(const string &fileName);

    // Open an index and return an ixfileHandle.
    RC openFile(const string &fileName, IXFileHandle &ixfileHandle);

    // Close an ixfileHandle for an index.
    RC closeFile(IXFileHandle &ixfileHandle);

    // Insert an entry into the given index that is indicated by the given ixfileHandle.
    RC insertEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);

    // Delete an entry from the given index that is indicated by the given ixfileHandle.
    RC deleteEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid);

    // Initialize and IX_ScanIterator to support a range search
    RC scan(IXFileHandle &ixfileHandle,
            const Attribute &attribute,
            const void *lowKey,
            const void *highKey,
            bool lowKeyInclusive,
            bool highKeyInclusive,
            IX_ScanIterator &ix_ScanIterator);

    // Print the B+ tree in pre-order (in a JSON record format)
    void printBtree(IXFileHandle &ixfileHandle, const Attribute &attribute) const;

    // public for scan
    RC readNode(IXFileHandle &ixfileHandle, const Attribute &attribute, unsigned pageNum, char*& page) const;
    RC loadLeafNode(const Attribute &attribute, LeafNode &node, unsigned pageNum, char* page) const;

protected:
    IndexManager();
    ~IndexManager();

private:
    static IndexManager *_index_manager;
    PagedFileManager *pfm;

    RC createIndexNode(IXFileHandle &ixfileHandle, const Attribute &attribute);
    RC createLeafNode(IXFileHandle &ixfileHandle, const Attribute &attribute);

    RC insertRoot(IXFileHandle &ixfileHandle, const Attribute &attribute, LeafNode &node, const void* key, const RID &rid);
    RC insertLeaf(IXFileHandle &ixfileHandle, const Attribute &attribute, LeafNode &node, const void* key, const RID &rid, vector< pair<unsigned, unsigned> > parent);
    RC insertIndex(IXFileHandle &ixfileHandle, const Attribute &attribute, const void* key, unsigned pageNum, vector< pair<unsigned, unsigned> > &parent);

    RC writeLeafNode(IXFileHandle &ixfileHandle, const Attribute &attribute, const LeafNode &node);
    RC writeIndexNode(IXFileHandle &ixfileHandle, const Attribute &attribute, const IndexNode &node);
    RC loadIndexNode(const Attribute &attribute, IndexNode &node, unsigned pageNum, char* page) const;

    RC searchIndex(IXFileHandle &ixfileHandle, const Attribute &attribute, const void* key, unsigned pageNum, vector< pair<unsigned, unsigned> > &parent, char*& page, unsigned& targetPageNum);

    bool isLeafNode(IXFileHandle &ixfileHandle, const Attribute &attribute, unsigned pageNum);

    RC splitRoot(IXFileHandle &ixfileHandle, const Attribute &attribute, LeafNode &root);
    RC splitLeaf(IXFileHandle &ixfileHandle, const Attribute &attribute, LeafNode &node, vector< pair<unsigned, unsigned> > &parent);
    RC splitIndex(IXFileHandle &ixfileHandle, const Attribute &attribute, IndexNode &node,
                     vector< pair<unsigned, unsigned> > &parent, unsigned insertPoint, const void* key, unsigned childPageNum);

    void updateLeafNode(LeafNode &node);
    void updateIndexNode(IndexNode &node);


    RC getKeysChildren(const void *page, const Attribute &attribute, IndexNode &node) const;

    RC getEntries(const void *page, const Attribute &attribute, LeafNode &node) const;

    void printIndexNode(IXFileHandle &ixfileHandle, const Attribute &attribute, IndexNode &root) const;
    void printLeafNode(IXFileHandle &ixfileHandle, const Attribute &attribute, LeafNode &node) const;

    // slot operations
    unsigned getNodeFlag(const void *page) const;
    unsigned getPageFreespace(const void *page) const;
    unsigned getSlotsNum(const void *page) const;
    unsigned getSlotInfo(const void *page, unsigned index) const;
    unsigned getNextPtr(const void *page) const;
    void setNodeFlag(const void *page, const unsigned num);
    void setPageFreespace(const void *page, const unsigned num);
    void setSlotNum(const void *page, const unsigned num);
    void setNextPtr(const void *page, const unsigned num);
    void setSlotInfo(const void *page, const unsigned index, const unsigned num);
};


class IX_ScanIterator {
    public:

		// Constructor
        IX_ScanIterator();

        // Destructor
        ~IX_ScanIterator();

        // Get next matching entry
        RC getNextEntry(RID &rid, void *key);

        // Terminate index scan
        RC close();

        RC setParams(IXFileHandle &ixfileHandle, const LeafNode& node, const unsigned pos, const Attribute attribute,
             const void *highKey, const bool highKeyInclusive, IndexManager* indexManager);
    private:
        bool isOpen;
        LeafNode _node;
        unsigned _pos;
        Attribute _attribute;
        IXFileHandle *_ixfileHandle;
        void *_highKey;
        bool _highkeyIsNull;
        bool _highKeyInclusive;
        IndexManager* _indexManager;
};



class IXFileHandle {
    public:

    // variables to keep counter for each operation
    unsigned ixReadPageCounter;
    unsigned ixWritePageCounter;
    unsigned ixAppendPageCounter;

    // Constructor
    IXFileHandle();

    // Destructor
    ~IXFileHandle();

	  // Put the current counter values of associated PF FileHandles into variables
	  RC collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount);

    FileHandle *_fileHandle;
};

#endif
