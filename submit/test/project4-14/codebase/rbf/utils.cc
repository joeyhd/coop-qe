#include <iostream>
#include <string>
#include <sys/stat.h>
#include <cassert>
#include <bitset>
#include <cstring>

#include "utils.h"

using std::string;

bool utils::FileExists(const string &fileName)
{
    struct stat stFileInfo;
    if(stat(fileName.c_str(), &stFileInfo) == 0) return true;
    else return false;
}

void utils::setShortIntAt(const void *data, const unsigned index, const unsigned num)
{
    assert(num < 2<<16);
    *(unsigned short*)((char*)data + index) = (unsigned short)num;
    // int offset = index;
    // *((char*)data+offset) = num & ((2<<8)-1); *((char*)data+offset+1) = num >> 8;
}

unsigned utils::getShortIntAt(const void *data, const unsigned index)
{
    return (unsigned)*(unsigned short*)((char*)data + index);
}

void utils::read(istream& is, void* value)
{
	utils::read(is, value, 1);
}

void utils::read(istream& is, void* value, const unsigned size, const unsigned offset)
{
    is.seekg(fstream::pos_type(offset));
    is.read((char*)value, size);
}

void utils::write(ostream& os, const void* value, const unsigned size, const unsigned offset)
{
    os.seekp(fstream::pos_type(offset));
    os.write((const char*)value, size);
}

void utils::printPage(char* page, unsigned size)
{
    unsigned cnt = 0;
    int leng = 32;
    while(cnt < size)
    {
        cout << cnt/leng << " : " ;
        if(cnt < size - leng)
        {
            for (int i = cnt; i < cnt+leng; ++i) {
                cout << (int)*(page+i) << " ";
            }
            cout << endl;
            cnt += leng;
        }
        else
        {
            for (int i = cnt; i < size; ++i) {
                cout << (int)*(page+i) << " ";
            }
            cout << endl;
            cnt += leng;
        }
    }
}

/*
bool utils::extractAttrFromData(const char* slot, const vector<Attribute>& iattrs,
      void* data, const string& oattrs) {
        const int fieldNum = iattrs.size();
        const int nullByteNum = (fieldNum-1)/8+1;
        const char *ptr = slot + nullByteNum;

        for (int i = 0; i < fieldNum; i++)
        {
            if (*((byte*)slot+i/8) & (128 >> i%8))
            {
                if(iattrs[i].name == oattrs)
                {
                    return false;
                }
                continue;
            }

            int curAttributeLen = 0;
            if (iattrs[i].type == TypeInt) curAttributeLen = 4;
            else if (iattrs[i].type == TypeReal) curAttributeLen = 4;
            else curAttributeLen = 4 + *((int*)ptr);
            if (iattrs[i].name == oattrs) {
                memcpy(data, ptr, curAttributeLen);
                return true;
            }
            ptr += curAttributeLen;
        }
        return false;
}
*/
void utils::extractAttrFromData(const void* slot, const vector<Attribute>& iattrs,
      void* data, const vector<string>& oattrs) {
        if (oattrs.size() == 0) return ;
        const int fieldNum = iattrs.size();
        const int nullByteNum = (fieldNum-1)/8+1;
        const char *ptr = (char*)slot + nullByteNum;
        // add null byte for data.
        memset(data, 0, nullByteNum);
        char *t_data = (char*)data + 1 + (oattrs.size()-1)/8;
        for (int j = 0; j < oattrs.size(); j++)
        {
            const char *t_ptr = ptr;
            for (int i = 0; i < fieldNum; i++)
            {
                if (*((byte*)slot+i/8) & (128 >> i%8))
                {
                    if(iattrs[i].name == oattrs[j])
                    {
                        *((char*)data+j/8) |= (128 >> (j%8));
                        return ;
                    }
                    continue;
                }

                int curAttributeLen = 0;
                if (iattrs[i].type == TypeInt) curAttributeLen = 4;
                else if (iattrs[i].type == TypeReal) curAttributeLen = 4;
                else curAttributeLen = 4 + *((int*)t_ptr);
                if (iattrs[i].name == oattrs[j]) {
                    memcpy(t_data, t_ptr, curAttributeLen);
                    t_data += curAttributeLen;
                    break;
                }
                t_ptr += curAttributeLen;
            }
        }
        return ;
}

Attribute utils::setAttribute(string name, AttrType type, AttrLength length)
{
    Attribute attr;
    attr.name = name;
    attr.type = type;
    attr.length = length;
    return attr;
}
Attribute utils::convertToAttribute(char* data)
{
    Attribute attr;
    char name[PAGE_SIZE] = {};
    memcpy(name, data+5, *((int*)(data+1)));
    attr.name = name;
    attr.type = *((AttrType*)(data+5+*((int*)(data+1))));
    attr.length = *((int*)(data+9+*((int*)(data+1))));
    return attr;
}

int utils::compare(const Attribute &attribute, const void* a, const void* b)
{
    if(attribute.type == TypeInt)
    {
        if(*(int*)a > *(int*)b) return 1;
        else if (*(int*)a < *(int*)b) return -1;
        else return 0;
    }
    if(attribute.type == TypeReal)
    {
        if(*(float*)a > *(float*)b) return 1;
        else if (*(float*)a < *(float*)b) return -1;
        else return 0;
    }
    if(attribute.type == TypeVarChar)
    {
        char page_a[PAGE_SIZE] = {}, page_b[PAGE_SIZE] = {};
        memcpy(page_a, (char*)a + 4, *(unsigned*)a);
        memcpy(page_b, (char*)b + 4, *(unsigned*)b);
        return (string(page_a).compare(string(page_b)));
    }
}

unsigned utils::getDataLength(const void *data, const Attribute &attribute)
{
    if(attribute.type == TypeInt || attribute.type == TypeReal) return 4;
    return (4+*(unsigned*)data);
}


dataGenerator::dataGenerator()
{ this->reset(); }

dataGenerator::~dataGenerator()
{ }

dataGenerator & dataGenerator::operator<<(const int val)
{
    *((int*)(this->_puredata + this->_curpos)) = val;
    this->_curpos += 4;
    this->_nullpos ++ ;
}
dataGenerator & dataGenerator::operator<<(const unsigned val)
{
    *((int*)(this->_puredata + this->_curpos)) = int(val);
    this->_curpos += 4;
    this->_nullpos ++ ;
}
dataGenerator & dataGenerator::operator<<(const float val)
{
    *((float*)(this->_puredata + this->_curpos)) = val;
    this->_curpos += 4;
    this->_nullpos ++ ;
}
dataGenerator & dataGenerator::operator<<(const double val)
{
    *((float*)(this->_puredata + this->_curpos)) = float(val);
    this->_curpos += 4;
    this->_nullpos ++ ;
}
dataGenerator & dataGenerator::operator<<(const string str)
{
    *((unsigned*)(this->_puredata + this->_curpos)) = str.length();
    memcpy(this->_puredata + this->_curpos + 4, str.c_str(), str.length());
    this->_curpos += (str.length() + 4);
    this->_nullpos ++ ;
}
dataGenerator & dataGenerator::operator<<(const data_null dn)
{
    this->_data[this->_nullpos/8] |= (128 >> (this->_nullpos%8));
    this->_nullpos ++ ;
}
RID dataGenerator::operator<<(const insflag insd)
{
    RID rid;
    memcpy(&this->_data[1+(this->_nullpos-1)/8], this->_puredata, this->_curpos);
    this->_rbfm->insertRecord(*this->_fh, this->_descriptor, this->_data, rid);
    this->reset();
    return rid;
}
void dataGenerator::setInserter(RecordBasedFileManager* rbfm, FileHandle* fh, vector<Attribute>& descriptor)
{
    this->_rbfm = rbfm;
    this->_fh = fh;
    this->_descriptor = descriptor;
}
void dataGenerator::reset()
{
    this->_curpos = 0;
    this->_nullpos = 0;
    memset(this->_data, 0, PAGE_SIZE);;
    memset(this->_puredata, 0, PAGE_SIZE);
}
