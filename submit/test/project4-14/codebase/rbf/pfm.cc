#include <iostream>
#include <fstream>
#include <cstdio>
#include <string>
#include <cstring>

#include "pfm.h"
#include "utils.h"

using namespace std;

PagedFileManager* PagedFileManager::_pf_manager = 0;

PagedFileManager* PagedFileManager::instance()
{
    if(!_pf_manager)
        _pf_manager = new PagedFileManager();

    return _pf_manager;
}


PagedFileManager::PagedFileManager()
{
}


PagedFileManager::~PagedFileManager()
{
}


RC PagedFileManager::createFile(const string &fileName)
{
    // check if file exists.
    if (utils::FileExists(fileName)) return FILE_EXISTS;
    // create file
    ofstream of;
    of.open(fileName.c_str(), ofstream::out); // using ios::binary in wc's code, is it better?
    this->initPagedFile(of);
    of.close();
    // check
    if (utils::FileExists(fileName)) return STD_SUCC;
    else return STD_ERR;
}


RC PagedFileManager::destroyFile(const string &fileName)
{
    if (!utils::FileExists(fileName)) return FILE_NOT_EXISTS;
    if (remove(fileName.c_str()) != 0) return STD_ERR;
    return STD_SUCC;
}


RC PagedFileManager::openFile(const string &fileName, FileHandle &fileHandle)
{
    return fileHandle.openFile(fileName);
}


RC PagedFileManager::closeFile(FileHandle &fileHandle)
{
    return fileHandle.closeFile();
}

RC PagedFileManager::initPagedFile(ofstream &of)
{
    // check
    if(!of.is_open()) return FILE_NOT_OPEN;
    if(!of) return FILE_STREAM_BROKEN;
    // init
    char catalogData[PAGE_SIZE] = {}; // all bytes in catalog page is set to 0, good as init.
    char indexData[PAGE_SIZE] = {};
    // write into file
    of.write(catalogData, PAGE_SIZE);
    of.write(indexData, PAGE_SIZE);
    return STD_SUCC;
}


FileHandle::FileHandle()
{
    readPageCounter = 0;
    writePageCounter = 0;
    appendPageCounter = 0;
    this->_readBuf = new char[PAGE_SIZE] {};
    this->_writeBuf = new char[PAGE_SIZE] {};
    this->_validBuf = true;
}


FileHandle::~FileHandle()
{
    if(this->isOpen()) this->closeFile();
}

void FileHandle::eraseBuf()
{
    if(!this->_validBuf) return;
    this->_validBuf = false;
    delete[] this->_readBuf;
    delete[] this->_writeBuf;
}

RC FileHandle::readPage(PageNum pageNum, void *data)
{
    if(!this->isOpen()) return FILE_NOT_OPEN;
    if(pageNum >= this->getNumberOfDataPages()) return ACCESS_OUT_BOUND;
    if(pageNum != this->_readBufNum && pageNum != this->_writeBufNum)
    {
        this->_readBufNum = pageNum;
        utils::read(this->fs, this->_readBuf, PAGE_SIZE, (pageNum + 2) * PAGE_SIZE);
    }
    else if(pageNum == this->_writeBufNum)
    {
        this->_readBufNum = this->_writeBufNum;
        memcpy(this->_readBuf, this->_writeBuf, PAGE_SIZE);
    }
    memcpy(data, this->_readBuf, PAGE_SIZE);
    this->readPageCounter ++;
    return STD_SUCC;
}


RC FileHandle::writePage(PageNum pageNum, const void *data)
{
    if(!this->isOpen()) return FILE_NOT_OPEN;
    if(pageNum >= this->getNumberOfDataPages()) return ACCESS_OUT_BOUND;
    this->writePageCounter ++;
    RC rc;
    if(this->_writeBufNum != pageNum && this->_writeBufNum != -1)
    {
        rc = this->actualWritePage(this->_writeBufNum, this->_writeBuf);
        if(rc != STD_SUCC) return rc;
        this->_writeBufNum = pageNum;
        memcpy(this->_writeBuf, data, PAGE_SIZE);
    } else
    {
        this->_writeBufNum = pageNum;
        memcpy(this->_writeBuf, data, PAGE_SIZE);
        if(this->_writeBufNum == this->_readBufNum)
            memcpy(this->_readBuf, this->_writeBuf, PAGE_SIZE);
    }
    return STD_SUCC;
}

RC FileHandle::actualWritePage(PageNum pageNum, const void *data)
{
    if(!this->isOpen()) return FILE_NOT_OPEN;
    if(pageNum >= this->getNumberOfDataPages()) return ACCESS_OUT_BOUND;
    utils::write(this->fs, data, PAGE_SIZE, (pageNum + 2) * PAGE_SIZE);
    return STD_SUCC;
}

RC FileHandle::appendPage(const void *data)
{
    if(!this->isOpen()) return FILE_NOT_OPEN;
    utils::write(this->fs, data, PAGE_SIZE, PAGE_SIZE*(2+this->pageNum));
    this->appendPageCounter ++;
    this->pageNum ++;
    this->indexPageNum = (this->pageNum-1)*2/PAGE_SIZE+1;
    if (this->indexPageNum > this->expandPageNum + 1)
    {
        this->expandPageNum ++;
        char* t_expandIndexData = new char[PAGE_SIZE*this->expandPageNum];
        if(this->expandPageNum > 1) {
            memcpy(t_expandIndexData, this->expandIndexData, PAGE_SIZE * (this->expandPageNum - 1));
            delete[](this->expandIndexData);
        }
        this->expandIndexData = t_expandIndexData;
        utils::write(this->fs, this->expandIndexData, PAGE_SIZE*(this->expandPageNum), PAGE_SIZE*(2+this->pageNum));
    }
    return STD_SUCC;
}


unsigned FileHandle::getNumberOfPages()
{
    return (this->pageNum + 1);
}


unsigned FileHandle::getNumberOfDataPages()
{
    return this->pageNum;
}


RC FileHandle::collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount)
{
    if(!this->isOpen()) return FILE_NOT_OPEN;
    readPageCount = this->readPageCounter;
    writePageCount = this->writePageCounter;
    appendPageCount = this->appendPageCounter;
    return STD_SUCC;
}

RC FileHandle::openFile(const string& fileName)
{
    // check file exists
    if (!utils::FileExists(fileName)) return FILE_NOT_EXISTS;
    if (this->isOpen()) return FILE_NOT_CLOSE;
    // open file
    this->fs.open(fileName.c_str(), fstream::in | fstream::out); // is it necessary to add binary?
    // read catalog data and index data
    char *hiddenPage = new char[PAGE_SIZE*2] {};
    this->fs.read(hiddenPage, PAGE_SIZE*2);
    // set ptr
    this->catalogData = hiddenPage; this->indexData = &hiddenPage[PAGE_SIZE];
    this->pageNum = *((unsigned*)this->catalogData);
    this->indexPageNum = *((unsigned*)this->catalogData+1);
    this->expandPageNum = this->indexPageNum > 0 ? this->indexPageNum-1 : 0;
    this->readPageCounter = *((unsigned*)this->catalogData+2);
    this->writePageCounter = *((unsigned*)this->catalogData+3);
    this->appendPageCounter = *((unsigned*)this->catalogData+4);
    // read expandPageNum
    if (this->expandPageNum > 0)
    {
        char *expandPage = new char[PAGE_SIZE*(this->expandPageNum)];
        utils::read(this->fs, expandPage, PAGE_SIZE*(this->expandPageNum), PAGE_SIZE*(2+this->pageNum));
        this->expandIndexData = expandPage;
    }
    // reset buf
    memset(this->_writeBuf, 0, PAGE_SIZE);
    this->_writeBufNum = -1;
    memset(this->_readBuf, 0, PAGE_SIZE);
    this->_readBufNum = -1;
    return STD_SUCC;
}

RC FileHandle::closeFile()
{
    // check if fs is closed
    if (!this->isOpen()) return FILE_NOT_OPEN;
    // write into catalog and index data.
    *((unsigned*)this->catalogData) = this->pageNum;
    *((unsigned*)this->catalogData + 1) = this->indexPageNum;
    *((unsigned*)this->catalogData + 2) = this->readPageCounter;
    *((unsigned*)this->catalogData + 3) = this->writePageCounter;
    *((unsigned*)this->catalogData + 4) = this->appendPageCounter;
    utils::write(this->fs, this->catalogData, PAGE_SIZE, 0);
    utils::write(this->fs, this->indexData, PAGE_SIZE, PAGE_SIZE);

    // write buf data
    if(this->_writeBufNum != -1) {
        RC rc = this->actualWritePage(this->_writeBufNum, this->_writeBuf);
        if (rc != STD_SUCC) return rc;
        this->_writeBufNum = -1;
    }
    // close file
    delete[](this->catalogData);
    if(this->expandPageNum > 0) {
        utils::write(this->fs, this->expandIndexData, PAGE_SIZE*(this->expandPageNum), PAGE_SIZE*(2+this->pageNum));
        delete[](this->expandIndexData);
    }
    this->fs.close();
    return STD_SUCC;
}

bool FileHandle::isOpen()
{
    return this->fs.is_open();
}

RC FileHandle::updateIndexData(const PageNum pageNum, const unsigned freespace)
{
    if (this->pageNum < pageNum) return STD_ERR;
    if (this->pageNum == pageNum) return STD_SUCC;
    if (pageNum < PAGE_SIZE/2)
        utils::setShortIntAt(this->indexData, pageNum*2, freespace);
    else
        utils::setShortIntAt(this->expandIndexData, pageNum*2-PAGE_SIZE, freespace);
    return STD_SUCC;
}

RC FileHandle::getFirstBigPage(const unsigned space, int &pageIndex)
{
    for (int i = 0; i < this->pageNum; i++)
    {
        int index = (i + this->pageNum - 1)%this->pageNum; // search last page first
        if(index < PAGE_SIZE/2)
        {
            if( utils::getShortIntAt(this->indexData, index*2) > space)
            {
                pageIndex = index;
                return STD_SUCC;
            }
        }
        else
        {
            if( utils::getShortIntAt(this->expandIndexData, 2*index-PAGE_SIZE) > space)
            {
                pageIndex = index;
                return STD_SUCC;
            }
        }
    }
    pageIndex = -1;
    return STD_SUCC;
}
