#ifndef _pfm_h_
#define _pfm_h_

typedef unsigned PageNum;
typedef int RC;
typedef char byte;

#define MAX_INDEX_GROUP 1024
#define PAGE_SIZE 4096
#include <string>
#include <climits>
#include <iostream>
#include <fstream>
using namespace std;

class FileHandle;

class PagedFileManager
{
public:
    static PagedFileManager* instance();                                  // Access to the _pf_manager instance

    RC createFile    (const string &fileName);                            // Create a new file
    RC destroyFile   (const string &fileName);                            // Destroy a file
    RC openFile      (const string &fileName, FileHandle &fileHandle);    // Open a file
    RC closeFile     (FileHandle &fileHandle);                            // Close a file

protected:
    PagedFileManager();                                                   // Constructor
    ~PagedFileManager();                                                  // Destructor

private:
    static PagedFileManager *_pf_manager;

    RC initPagedFile(ofstream &of);
};


class FileHandle
{
public:
    // variables to keep the counter for each operation
    unsigned readPageCounter;
    unsigned writePageCounter;
    unsigned appendPageCounter;

    FileHandle();                                                         // Default constructor
    ~FileHandle();                                                        // Destructor

    RC readPage(PageNum pageNum, void *data);                             // Get a specific page
    RC writePage(PageNum pageNum, const void *data);                      // Write a specific page
    RC appendPage(const void *data);                                      // Append a specific page
    unsigned getNumberOfPages();                                          // Get the number of pages in the file
    unsigned getNumberOfDataPages();
    RC collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount);  // Put the current counter values into variables

    // added functions
    RC openFile(const string& fileName);
    RC closeFile();
    RC getFirstBigPage(const unsigned space, int &pageIndex);
    RC updateIndexData(const PageNum pageNum, const unsigned freespace);
    void eraseBuf();
private:
    char *catalogData;
    char *indexData, *expandIndexData;
    char* _readBuf; int _readBufNum;
    char* _writeBuf; int _writeBufNum;
    bool _validBuf;
    unsigned curExpandIndexGroup, totalExpandPageGroupNum;
    unsigned pageNum, indexPageNum, expandPageNum;
    bool isOpen();
    RC actualWritePage(PageNum pageNum, const void *data);
    fstream fs;                                                           // keep private
};

#endif
