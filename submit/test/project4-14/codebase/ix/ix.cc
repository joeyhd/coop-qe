
#include <cassert>
#include <cstring>
#include <iostream>
#include "ix.h"
#include "../rbf/utils.h"
#include "../rbf/rbfm.h"

IndexManager* IndexManager::_index_manager = 0;


void IndexNode::operator=(const IndexNode& b)
{
    this->freeSpace = b.freeSpace;
    this->numOfSlots = b.numOfSlots;
    this->pageNum = b.pageNum;
    memcpy(this->page, b.page, PAGE_SIZE);
    this->keys.clear();
    for (int i = 0; i < b.keys.size(); ++i)
    {
         this->keys.push_back(this->page+(int)((char*)b.keys[i]-b.page));
    }
    this->children = b.children;
}

void LeafNode::operator=(const LeafNode& b)
{
    this->freeSpace = b.freeSpace;
    this->numOfSlots = b.numOfSlots;
    this->pageNum = b.pageNum;
    this->next = b.next;
    memcpy(this->page, b.page, PAGE_SIZE);
    this->entries.clear();
    for (int i = 0; i < b.entries.size(); ++i)
    {
        this->entries.push_back(make_pair(this->page+(int)((char*)b.entries[i].first-b.page), b.entries[i].second));
    }
}

IndexManager* IndexManager::instance()
{
    if(!_index_manager)
        _index_manager = new IndexManager();

    return _index_manager;
}

IndexManager::IndexManager()
{
    this->pfm = PagedFileManager::instance();
}

IndexManager::~IndexManager()
{
}

RC IndexManager::createFile(const string &fileName)
{
    return this->pfm->createFile(fileName);
}

RC IndexManager::destroyFile(const string &fileName)
{
    return this->pfm->destroyFile(fileName);
}

RC IndexManager::openFile(const string &fileName, IXFileHandle &ixfileHandle)
{
    return this->pfm->openFile(fileName, *ixfileHandle._fileHandle);
}

RC IndexManager::closeFile(IXFileHandle &ixfileHandle)
{
    return this->pfm->closeFile(*ixfileHandle._fileHandle);
}

RC IndexManager::insertEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid)
{
    // initialize root as a leaf node
    // root pageNum is fixed to 0
    if (ixfileHandle._fileHandle->getNumberOfDataPages() == 0) {
        RC rc = this->createLeafNode(ixfileHandle, attribute);
        if(rc != STD_SUCC) return rc;
    }
    if (ixfileHandle._fileHandle->getNumberOfDataPages() == 1) {
        LeafNode root;
        RC rc = this->readNode(ixfileHandle, attribute, 0, root.page);
        if(rc != STD_SUCC) return rc;
        rc = this->loadLeafNode(attribute, root, 0, root.page);
        if(rc != STD_SUCC) return rc;
        return this->insertRoot(ixfileHandle, attribute, root, key, rid);
    } else {
        vector< pair<unsigned, unsigned> > parent;
        unsigned pageNum;
        LeafNode node;
        this->searchIndex(ixfileHandle, attribute, key, 0, parent, node.page, pageNum);
        RC rc = this->loadLeafNode(attribute, node, pageNum, node.page);
        if(rc != STD_SUCC) return rc;
        return this->insertLeaf(ixfileHandle, attribute, node, key, rid, parent);
    }
}

RC IndexManager::insertRoot(IXFileHandle &ixfileHandle, const Attribute &attribute, LeafNode &node, const void* key, const RID &rid) {
    if (node.entries.size() == MAX_SLOT || node.freeSpace < (utils::getDataLength(key, attribute) + 8))
    {
        RC rc = this->splitRoot(ixfileHandle, attribute, node);
        if(rc != STD_SUCC) return rc;
        return this->insertEntry(ixfileHandle, attribute, key, rid);
    }
    int size = node.numOfSlots;
    int pos = 0;
    for (int i = 0; i < size; i++) {
        if (utils::compare(attribute, node.entries[i].first, key) >= 0) break;
        else pos++;
    }

    // update node.and node page
    int dataLength = utils::getDataLength(key, attribute);
    void* end = node.page;
    if(size > 0) end = (char*)node.entries[size-1].first + utils::getDataLength(node.entries[size-1].first, attribute) + 8;
    node.entries.push_back(make_pair(end, rid));
    for (int i = size; i > pos; i--)
    {
        memmove((char*)node.entries[i-1].first+dataLength+8, node.entries[i-1].first, utils::getDataLength(node.entries[i-1].first, attribute)+8);
        node.entries[i].first = (char*)node.entries[i-1].first + dataLength + 8;
        node.entries[i].second = node.entries[i-1].second;
    }
    memcpy(node.entries[pos].first, key, dataLength);
    *(unsigned*)((char*)node.entries[pos].first+dataLength) = rid.pageNum;
    *(unsigned*)((char*)node.entries[pos].first + dataLength + 4) = (unsigned)rid.slotNum;
    node.entries[pos].second = rid;
    this->updateLeafNode(node);
    node.freeSpace -= ( utils::getDataLength(key, attribute) + 8 );
    return this->writeLeafNode(ixfileHandle, attribute, node);

}

RC IndexManager::insertLeaf(IXFileHandle &ixfileHandle, const Attribute &attribute, LeafNode &node, const void* key, const RID &rid, vector< pair<unsigned, unsigned> > parent) {
    if (node.entries.size() == MAX_SLOT || node.freeSpace < (utils::getDataLength(key, attribute) + 8))
    {
        RC rc = this->splitLeaf(ixfileHandle, attribute, node, parent);
        if(rc != STD_SUCC) return rc;
        return this->insertEntry(ixfileHandle, attribute, key, rid);
    }
    int size = node.numOfSlots;
    int pos = 0;
    // insertion can be optimized using binary search
    for (int i = 0; i < size; i++) {
        if (utils::compare(attribute, node.entries[i].first, key) >= 0) break;
        else pos++;
    }

    // update node and node page
    int dataLength = utils::getDataLength(key, attribute);
    void* end = node.page;
    if(size > 0) end = (char*)node.entries[size-1].first + utils::getDataLength(node.entries[size-1].first, attribute) + 8;
    node.entries.push_back(make_pair(end, rid));
    for (int i = size; i > pos; i--)
    {
        memmove((char*)node.entries[i-1].first+dataLength+8, node.entries[i-1].first, utils::getDataLength(node.entries[i-1].first, attribute)+8);
        node.entries[i].first = (char*)node.entries[i-1].first + dataLength + 8;
        node.entries[i].second = node.entries[i-1].second;
    }
    memcpy(node.entries[pos].first, key, dataLength);
    *(unsigned*)((char*)node.entries[pos].first+dataLength) = rid.pageNum;
    *(unsigned*)((char*)node.entries[pos].first + dataLength + 4) = (unsigned)rid.slotNum;
    node.entries[pos].second = rid;

    this->updateLeafNode(node);
    node.freeSpace -= ( utils::getDataLength(key, attribute) + 8 );
    return this->writeLeafNode(ixfileHandle, attribute, node);  
}

RC IndexManager::insertIndex(IXFileHandle &ixfileHandle, const Attribute &attribute, const void* key, unsigned childPageNum, vector< pair<unsigned, unsigned> >& parent) {
    pair<unsigned, unsigned> parentNodeIndex = parent.back();
    parent.pop_back();
    unsigned parentPageNum = parentNodeIndex.first;
    unsigned parentSlotnum = parentNodeIndex.second;
    IndexNode parentNode;
    RC rc = this->readNode(ixfileHandle, attribute, parentPageNum, parentNode.page);
    if (rc != STD_SUCC) return rc;
    rc = this->loadIndexNode(attribute, parentNode, parentPageNum, parentNode.page);
    if(rc != STD_SUCC) return rc;
    if(parentNode.keys.size() == MAX_SLOT or parentNode.freeSpace < (utils::getDataLength(key, attribute) + 4))
    {
        return this->splitIndex(ixfileHandle, attribute, parentNode, parent, parentNodeIndex.second, key, childPageNum);
    }

    // update node and node page
    int size = parentNode.keys.size();
    int dataLength = utils::getDataLength(key, attribute);
    void* end = (void*)(4 + parentNode.page);
    if(size > 0) end = (void*)((char*)parentNode.keys[size-1] + utils::getDataLength(parentNode.keys[size-1], attribute) + 4);
    parentNode.keys.push_back(end); parentNode.children.push_back(childPageNum);
    for (int i = size; i > parentSlotnum; i--)
    {
        memmove((char*)parentNode.keys[i-1]+dataLength+4, parentNode.keys[i-1], utils::getDataLength(parentNode.keys[i-1], attribute)+4);
        parentNode.keys[i] = (char*)parentNode.keys[i-1] + dataLength + 4;
        parentNode.children[i+1] = parentNode.children[i];
    }
    memcpy(parentNode.keys[parentSlotnum], key, dataLength);
    *(unsigned*)((char*)parentNode.keys[parentSlotnum]+dataLength) = childPageNum;
    parentNode.children[parentSlotnum+1] = childPageNum;
    parentNode.freeSpace -= (utils::getDataLength(key, attribute) + 4);
    this->updateIndexNode(parentNode);

    // maintain parentNode
    return this->writeIndexNode(ixfileHandle, attribute, parentNode);

}

RC IndexManager::deleteEntry(IXFileHandle &ixfileHandle, const Attribute &attribute, const void *key, const RID &rid)
{
    vector< pair<unsigned, unsigned> > parent;
    unsigned pageNum;
    LeafNode node;
    RC rc = this->searchIndex(ixfileHandle, attribute, key, 0, parent, node.page, pageNum);
    if(rc != STD_SUCC) return rc;
    rc = this->loadLeafNode(attribute, node, pageNum, node.page); if(rc != STD_SUCC) return rc;
    while(true) {
        for (unsigned pos = 0; pos < node.entries.size(); pos++) {
            if (utils::compare(attribute, node.entries[pos].first, key) == 0 &&
                node.entries[pos].second.pageNum == rid.pageNum &&
                node.entries[pos].second.slotNum == rid.slotNum) {
                unsigned moveLength = utils::getDataLength(node.entries[pos].first, attribute) + 8;
                for (int i = pos; i < node.entries.size() - 1; ++i) {
                    unsigned dataLength = utils::getDataLength(node.entries[i + 1].first, attribute) + 8;
                    node.entries[i].first = (char *) node.entries[i + 1].first - moveLength;
                    node.entries[i].second = node.entries[i + 1].second;
                    memcpy(node.entries[i].first, node.entries[i + 1].first, dataLength);
                }
                node.entries.pop_back();
                this->updateLeafNode(node);
                node.freeSpace += (utils::getDataLength(key, attribute) + 8);
                return this->writeLeafNode(ixfileHandle, attribute, node);
            }
            if (utils::compare(attribute, node.entries[pos].first, key) > 0)
            {
                return NON_EXIST_ENTRY;
            }
        }
        if(node.next == 0)return NON_EXIST_ENTRY;
        pageNum = node.next;
        rc = this->readNode(ixfileHandle, attribute, pageNum, node.page);
        if(rc != STD_SUCC) return rc;
        rc = this->loadLeafNode(attribute, node, pageNum, node.page);
        if(rc != STD_SUCC) return rc;
    }
}


RC IndexManager::scan(IXFileHandle &ixfileHandle,
        const Attribute &attribute,
        const void      *lowKey,
        const void      *highKey,
        bool			lowKeyInclusive,
        bool        	highKeyInclusive,
        IX_ScanIterator &ix_ScanIterator)
{
    LeafNode node;
    vector< pair<unsigned, unsigned> > parent;
    unsigned pageNum;
    RC rc = this->searchIndex(ixfileHandle, attribute, lowKey, 0, parent, node.page, pageNum);
    if(rc != STD_SUCC) return rc;
    rc = this->loadLeafNode(attribute, node, pageNum, node.page);
    if(rc != STD_SUCC) return rc;
    unsigned pos = 0;
    for( ; pos < node.entries.size(); pos++)
    {
        if(lowKey == NULL) break;
        if(lowKeyInclusive && utils::compare(attribute, node.entries[pos].first, lowKey) >= 0) break;
        if(!lowKeyInclusive && utils::compare(attribute, node.entries[pos].first, lowKey) > 0) break;
    }
    return ix_ScanIterator.setParams(ixfileHandle, node, pos, attribute, highKey, highKeyInclusive, this);
}

void IndexManager::printBtree(IXFileHandle &ixfileHandle, const Attribute &attribute) const {
    if (ixfileHandle._fileHandle->getNumberOfDataPages() == 1) {
        LeafNode root;
        RC rc;
        rc = this->readNode(ixfileHandle, attribute, 0, root.page);
        if (rc != STD_SUCC) return;
        rc = this->loadLeafNode(attribute, root, 0, root.page);
        if (rc != STD_SUCC) return;
        this->printLeafNode(ixfileHandle, attribute, root);
    } else {
        IndexNode root;
        RC rc;
        rc = this->readNode(ixfileHandle, attribute, 0, root.page);
        if (rc != STD_SUCC) return;
        rc = this->loadIndexNode(attribute, root, 0, root.page);
        if (rc != STD_SUCC) return;
        this->printIndexNode(ixfileHandle, attribute, root);
    }
}

void IndexManager::printIndexNode(IXFileHandle &ixfileHandle, const Attribute &attribute, IndexNode &root) const {
    cout << "{\"keys\":[";
    for (vector<void *>::iterator i = root.keys.begin(); i != root.keys.end(); ++i) {
        if (attribute.type == TypeInt) {
            int key = *((int *)*i);
            if (i < root.keys.end() - 1)
                cout << key << ",";
            else
                cout << key;
        } else if (attribute.type == TypeReal) {
            float key = *((float *)*i);
            if (i < root.keys.end() - 1)
                cout << key << ",";
            else
                cout << key;
        } else {
            char key[PAGE_SIZE] = {};
            memcpy(key, (char*)*i + 4, *(unsigned*)(*i));
            if (i < root.keys.end() - 1)
                cout << "\"" << string(key) << "\",";
            else
                cout << "\"" << string(key) << "\"";
        }
    }
    cout << "]," << endl;
    cout << "\"children\":[" << endl;
    for (vector<unsigned>::iterator i = root.children.begin(); i != root.children.end(); ++i) {
        LeafNode Leafchild;
        RC rc = this->readNode(ixfileHandle, attribute, *i, Leafchild.page);
        if (rc != STD_SUCC) return;
        if(this->getNodeFlag(Leafchild.page) == 1) {
            rc = this->loadLeafNode(attribute, Leafchild, *i, Leafchild.page);
            if (rc != STD_SUCC) return;
            this->printLeafNode(ixfileHandle, attribute, Leafchild);
        } else {
            IndexNode Indexchild;
            memcpy(Indexchild.page, Leafchild.page, PAGE_SIZE);
            rc = this->loadIndexNode(attribute, Indexchild, *i, Indexchild.page);
            if (rc != STD_SUCC) return;
            this->printIndexNode(ixfileHandle, attribute, Indexchild);
        }
        if (i < root.children.end() - 1) cout << "," << endl;
    }
    cout << "]}";
}

void IndexManager::printLeafNode(IXFileHandle &ixfileHandle, const Attribute &attribute, LeafNode &node) const {
    cout << "{\"keys\":[";
    for (vector<  pair<void*, RID> >::iterator i = node.entries.begin(); i != node.entries.end(); ++i) {
        pair<void*, RID> entry = *i;
        if (attribute.type == TypeInt) {
            if (i < node.entries.end() - 1)
                cout << "\"" << *((int *)entry.first) << ":" << "[(" << entry.second.pageNum << "," << entry.second.slotNum << ")]\"" << ",";
            else
                cout << "\"" << *((int *)entry.first) << ":" << "[(" << entry.second.pageNum << "," << entry.second.slotNum << ")]\"";
        } else if (attribute.type == TypeReal) {
            if (i < node.entries.end() - 1)
                cout << "\"" << *((float *)entry.first) << ":" << "[(" << entry.second.pageNum << "," << entry.second.slotNum << ")]\"" << ",";
            else
                cout << "\"" << *((float *)entry.first) << ":" << "[(" << entry.second.pageNum << "," << entry.second.slotNum << ")]\"";
        } else {
            char key[PAGE_SIZE] = {};
            memcpy(key, (char*)entry.first + 4, *(unsigned*)(entry.first));
            if (i < node.entries.end() - 1)
                cout << "\"" << string(key) << ":" << "[(" << entry.second.pageNum << "," << entry.second.slotNum << ")]\"" << ",";
            else
                cout << "\"" << string(key) << ":" << "[(" << entry.second.pageNum << "," << entry.second.slotNum << ")]\"";
        }
    }
    cout << "]}" << endl;
}

RC IndexManager::createIndexNode(IXFileHandle &ixfileHandle, const Attribute &attribute) {
    unsigned pageNum = ixfileHandle._fileHandle->getNumberOfDataPages();
    char page[PAGE_SIZE] = {};
    ixfileHandle._fileHandle->appendPage(page);
    IndexNode node;
    memcpy(node.page, page, PAGE_SIZE);
    node.freeSpace = PAGE_SIZE - 6;
    node.numOfSlots = 0;
    node.pageNum = pageNum;
    return this->writeIndexNode(ixfileHandle, attribute, node);
}

RC IndexManager::createLeafNode(IXFileHandle &ixfileHandle, const Attribute &attribute) {
    unsigned pageNum = ixfileHandle._fileHandle->getNumberOfDataPages();
    char page[PAGE_SIZE] = {};
    ixfileHandle._fileHandle->appendPage(page);
    LeafNode node;
    memcpy(node.page, page, PAGE_SIZE);
    node.freeSpace = PAGE_SIZE - 6;
    node.numOfSlots = 0;
    node.pageNum = pageNum;
    return this->writeLeafNode(ixfileHandle, attribute, node);
}

RC IndexManager::searchIndex(IXFileHandle &ixfileHandle, const Attribute &attribute, const void* key, unsigned pageNum,
                                   vector< pair<unsigned, unsigned> > &parent, char*& page, unsigned &targetPageNum) {
    IndexNode node;
    RC rc = this->readNode(ixfileHandle, attribute, pageNum, page);
    if (rc != STD_SUCC) return rc;
    if(this->getNodeFlag(page) == 1)
    {
        targetPageNum = pageNum;
        return STD_SUCC;
    } // is leaf node
    memcpy(node.page, page, PAGE_SIZE);
    rc = this->loadIndexNode(attribute, node, pageNum, page);
    if(rc != STD_SUCC) return rc;
    int size = node.keys.size();
    for (int i = 0; i < size; i++) {
        if (key == NULL or utils::compare(attribute, node.keys[i], key) >= 0) {
            parent.push_back(make_pair(node.pageNum, i));
            return this->searchIndex(ixfileHandle, attribute, key, node.children[i], parent, page, targetPageNum);
        }
    }
    parent.push_back(make_pair(node.pageNum, size));
    return this->searchIndex(ixfileHandle, attribute, key, node.children[size], parent, page, targetPageNum);
}

RC IndexManager::splitRoot(IXFileHandle &ixfileHandle, const Attribute &attribute, LeafNode &root) {
    LeafNode node[2];
    unsigned newPageNum1 = ixfileHandle._fileHandle->getNumberOfDataPages();
    RC rc = this->createLeafNode(ixfileHandle, attribute); if(rc != STD_SUCC) return rc;
    rc = this->readNode(ixfileHandle, attribute, newPageNum1, node[0].page);
    if(rc != STD_SUCC) return rc;
    rc = this->loadLeafNode(attribute, node[0], newPageNum1, node[0].page);
    if(rc != STD_SUCC) return rc;
    unsigned newPageNum2 = ixfileHandle._fileHandle->getNumberOfDataPages();
    rc = this->createLeafNode(ixfileHandle, attribute); if(rc != STD_SUCC) return rc;
    rc = this->readNode(ixfileHandle, attribute, newPageNum2, node[1].page);
    if(rc != STD_SUCC) return rc;
    rc = this->loadLeafNode(attribute, node[1], newPageNum2, node[1].page);
    if(rc != STD_SUCC) return rc;

    // move data, update nodes
    int size = root.entries.size();
    for (int j = 0; j < 2; j++)
    {
        int offset = 0;
        int start = 0, end = 0;
        if(j == 0) {start = 0; end = size/2;}
        if(j == 1) {start = size/2; end = size;}
        for (int i = start; i < end; i++) {
            int dataLength = utils::getDataLength((char*)root.entries[i].first, attribute);
            memcpy(node[j].page+offset, root.entries[i].first, dataLength);
            void* key = (void*)(node[j].page + offset);
            offset += dataLength;
            *(unsigned *)(node[j].page + offset) = root.entries[i].second.pageNum;
            *(unsigned *) (node[j].page + offset + 4) = root.entries[i].second.slotNum;
            RID rid = root.entries[i].second;
            offset += 8;
            node[j].entries.push_back(make_pair(key, rid));
            node[j].freeSpace -= (utils::getDataLength(key, attribute) + 8);
        }
    }
    node[0].next = node[1].pageNum;
    this->updateLeafNode(node[0]);
    this->updateLeafNode(node[1]);

    // convert root from leaf to index
    IndexNode newRoot;
    memcpy(newRoot.page, root.page, PAGE_SIZE);
    memset(root.page, 0, PAGE_SIZE);
    newRoot.pageNum = 0;
    newRoot.freeSpace = PAGE_SIZE - 6;
    int dataLength = utils::getDataLength((char*)node[1].entries[0].first, attribute);
    memcpy(newRoot.page + 4, node[1].entries[0].first, dataLength);
    newRoot.freeSpace -= utils::getDataLength(node[1].entries[0].first, attribute);
    *(unsigned *)newRoot.page = node[0].pageNum;
    *(unsigned *)(newRoot.page + 4 + dataLength) = node[1].pageNum;
    newRoot.keys.push_back(newRoot.page + 4);
    newRoot.children.push_back(node[0].pageNum);
    newRoot.children.push_back(node[1].pageNum);
    newRoot.freeSpace -= 8;
    this->updateIndexNode(newRoot);

    // write
    rc = this->writeIndexNode(ixfileHandle, attribute, newRoot); if(rc != STD_SUCC) return rc;
    rc = this->writeLeafNode(ixfileHandle, attribute, node[0]); if(rc != STD_SUCC) return rc;
    return this->writeLeafNode(ixfileHandle, attribute, node[1]);
}

RC IndexManager::splitLeaf(IXFileHandle &ixfileHandle, const Attribute &attribute, LeafNode &node, vector< pair<unsigned, unsigned> > &parent) {
    unsigned newPageNum = ixfileHandle._fileHandle->getNumberOfDataPages();
    RC rc = this->createLeafNode(ixfileHandle, attribute); if(rc != STD_SUCC) return rc;
    LeafNode newNode;
    rc = this->readNode(ixfileHandle, attribute, newPageNum, newNode.page);
    if(rc != STD_SUCC) return rc;
    rc = this->loadLeafNode(attribute, newNode, newPageNum, newNode.page);
    if(rc != STD_SUCC) return rc;
    int size = node.entries.size();
    int offset = 0;
    for (int i = size/2; i < size; i++) {
        int dataLength = utils::getDataLength((char*)node.entries[i].first, attribute);
        memcpy(newNode.page+offset, node.entries[i].first, dataLength);
        void* key = (void*)(newNode.page + offset);
        offset += dataLength;
        *(unsigned *)(newNode.page + offset) = node.entries[i].second.pageNum;
        *(unsigned *) (newNode.page + offset + 4) = node.entries[i].second.slotNum;
        RID rid = node.entries[i].second;
        offset += 8;
        newNode.entries.push_back(make_pair(key, rid));
        node.freeSpace += (utils::getDataLength(key, attribute) + 8);
        newNode.freeSpace -= (utils::getDataLength(key, attribute) + 8);
    }
    node.entries.erase(node.entries.begin()+size/2, node.entries.end());
    newNode.next = node.next;
    node.next = newPageNum;
    // write updated nodes
    this->updateLeafNode(node);
    this->updateLeafNode(newNode);
    rc = this->writeLeafNode(ixfileHandle, attribute, node); if(rc != STD_SUCC) return rc;
    rc = this->writeLeafNode(ixfileHandle, attribute, newNode); if(rc != STD_SUCC) return rc;
    // copy up to parent index node
    return this->insertIndex(ixfileHandle, attribute, newNode.entries[0].first, newPageNum, parent);
}

RC IndexManager::splitIndex(IXFileHandle &ixfileHandle, const Attribute &attribute, IndexNode &node,
                     vector< pair<unsigned, unsigned> > &parent, unsigned insertPoint, const void* key, unsigned childPageNum) {
    if(parent.empty()) // current node is root node
    {
        unsigned newPageNum = ixfileHandle._fileHandle->getNumberOfDataPages();
        RC rc = this->createIndexNode(ixfileHandle, attribute); if(rc != STD_SUCC) return rc;
        IndexNode newNode;
        rc = this->readNode(ixfileHandle, attribute, newPageNum, newNode.page);
        if(rc != STD_SUCC) return rc;
        rc = this->loadIndexNode(attribute, newNode, newPageNum, newNode.page);
        if(rc != STD_SUCC) return rc;
        *(unsigned*)(newNode.page) = node.children[0];
        newNode.children.push_back(node.children[0]);
        newNode.freeSpace -= 4;

        int size = node.keys.size();

        int offset = 4;
        for (int i = 0; i < size; ++i)
        {
            int dataLength = utils::getDataLength((char*)node.keys[i], attribute);
            memcpy(newNode.page+offset, node.keys[i], dataLength);
            void* key = (void*)(newNode.page + offset);
            offset += dataLength;
            *(unsigned *)(newNode.page + offset) = node.children[i+1];
            offset += 4;
            newNode.keys.push_back(key);
            newNode.children.push_back(node.children[i+1]);
            newNode.freeSpace -= (utils::getDataLength(key, attribute) + 4);
        }

        node.children.clear();
        node.keys.clear();
        node.freeSpace = PAGE_SIZE - 6;
        node.children.push_back(newNode.pageNum);
        node.freeSpace -= 4;
        *(unsigned*)node.page = newNode.pageNum;
        this->updateIndexNode(node);
        this->updateIndexNode(newNode);
        this->writeIndexNode(ixfileHandle, attribute, node);

        IndexNode tmp;
        tmp = node;
        node = newNode;
        newNode = tmp;
    }
    unsigned newPageNum = ixfileHandle._fileHandle->getNumberOfDataPages();
    RC rc = this->createIndexNode(ixfileHandle, attribute); if(rc != STD_SUCC) return rc;
    IndexNode newNode;
    rc = this->readNode(ixfileHandle, attribute, newPageNum, newNode.page);
    if(rc != STD_SUCC) return rc;
    rc = this->loadIndexNode(attribute, newNode, newPageNum, newNode.page);
    if(rc != STD_SUCC) return rc;
    int size = node.keys.size();
    int middle = size / 2;
    void* middleKey = node.keys[middle];
    node.freeSpace += (utils::getDataLength(middleKey, attribute) + 4);

    *(unsigned*)(newNode.page) = node.children[middle+1];
    newNode.children.push_back(node.children[middle+1]);
    newNode.freeSpace -= 4;
    unsigned offset = 4;
    for (int i = 1+middle; i < size; ++i)
    {
        unsigned dataLength = utils::getDataLength((char*)node.keys[i], attribute);
        memcpy(newNode.page+offset, node.keys[i], dataLength);
        void* key = (void*)(newNode.page + offset);
        offset += dataLength;
        *(unsigned *)(newNode.page + offset) = node.children[i+1];
        offset += 4;
        newNode.keys.push_back(key);
        newNode.children.push_back(node.children[i+1]);
        newNode.freeSpace -= (utils::getDataLength(key, attribute) + 4);
        node.freeSpace += (utils::getDataLength(key, attribute) + 4);
    }

    node.keys.erase(node.keys.begin() + middle, node.keys.end());
    node.children.erase(node.children.begin() + middle + 1, node.children.end());

    // write node after split
    this->updateIndexNode(node);
    this->updateIndexNode(newNode);
    rc = this->writeIndexNode(ixfileHandle, attribute, node); if(rc != STD_SUCC) return rc;
    rc = this->writeIndexNode(ixfileHandle, attribute, newNode); if(rc != STD_SUCC) return rc;

    // insert middle key to parent node
    if (parent.empty()) parent.push_back(make_pair(0, 0));
    if(insertPoint <= middle) parent.push_back(make_pair(node.pageNum, insertPoint));
    else parent.push_back(make_pair(newNode.pageNum, insertPoint - middle-1));
    rc = this->insertIndex(ixfileHandle, attribute, key, childPageNum, parent);
    if (rc != STD_SUCC) return rc;
    return this->insertIndex(ixfileHandle, attribute, middleKey, newPageNum, parent);
}

void IndexManager::updateIndexNode(IndexNode &node) {
    // update index node numOfSlots and freespace based on keys
    int size = node.keys.size();
    node.numOfSlots = size;
}

void IndexManager::updateLeafNode(LeafNode &node) {
    // update leaf node numOfSlots and freespace based on data entries
    int size = node.entries.size();
    node.numOfSlots = size;
}

RC IndexManager::writeIndexNode(IXFileHandle &ixfileHandle, const Attribute &attribute, const IndexNode &node) {
    this->setPageFreespace(node.page, node.freeSpace);
    this->setSlotNum(node.page, node.numOfSlots);
    this->setNodeFlag(node.page, 0);
    return ixfileHandle._fileHandle->writePage(node.pageNum, node.page);
}

RC IndexManager::writeLeafNode(IXFileHandle &ixfileHandle, const Attribute &attribute, const LeafNode &node) {
    this->setPageFreespace(node.page, node.freeSpace);
    this->setSlotNum(node.page, node.numOfSlots);
    this->setNextPtr(node.page, node.next);
    this->setNodeFlag(node.page, 1);
    return ixfileHandle._fileHandle->writePage(node.pageNum, node.page);
}

RC IndexManager::readNode(IXFileHandle &ixfileHandle, const Attribute &attribute, unsigned pageNum, char*& page) const {
    return ixfileHandle._fileHandle->readPage(pageNum, page);
}

RC IndexManager::loadIndexNode(const Attribute &attribute, IndexNode &node, unsigned pageNum, char* page) const {
    node.freeSpace = this->getPageFreespace(page);
    node.numOfSlots = this->getSlotsNum(page);
    node.pageNum = pageNum;
    return this->getKeysChildren(page, attribute, node);
}

RC IndexManager::loadLeafNode(const Attribute &attribute, LeafNode &node, unsigned pageNum, char* page) const {
    node.freeSpace = this->getPageFreespace(page);
    node.numOfSlots = this->getSlotsNum(page);
    node.pageNum = pageNum;
    node.next = this->getNextPtr(page);
    return this->getEntries(page, attribute, node);
}

void IndexManager::setNodeFlag(const void *page, const unsigned num) {
    this->setSlotInfo(page, 0, num);
}

void IndexManager::setPageFreespace(const void *page, const unsigned num)
{ this->setSlotInfo(page, 1, num); }

void IndexManager::setSlotNum(const void *page, const unsigned num)
{ this->setSlotInfo(page, 2, num); }

void IndexManager::setSlotInfo(const void *page, const unsigned index, const unsigned num)
{
    utils::setShortIntAt(page, PAGE_SIZE-2*index-2, num);
}

void IndexManager::setNextPtr(const void *page, const unsigned num) {
    *(unsigned *)((char *)page + PAGE_SIZE - 10) = num;
}

unsigned IndexManager::getNodeFlag(const void *page) const {
    return this->getSlotInfo(page, 0);
}

unsigned IndexManager::getPageFreespace(const void *page) const
{ return(this->getSlotInfo(page, 1)); }

unsigned IndexManager::getSlotsNum(const void *page) const
{ return(this->getSlotInfo(page, 2)); }

unsigned IndexManager::getSlotInfo(const void *page, unsigned index) const
{
    return utils::getShortIntAt(page, PAGE_SIZE-2*index-2);
}

unsigned IndexManager::getNextPtr(const void *page) const {
    return *(unsigned *)((char *)page + PAGE_SIZE - 10);
}

RC IndexManager::getKeysChildren(const void *page, const Attribute &attribute, IndexNode &node) const {
    int size = node.numOfSlots;
    int offset = 0;
    for (int i = 0; i < size; i++) {
        unsigned child = *((unsigned *)((char *)page + offset));
        offset += 4;
        void* key = (void*)((char*)page + offset);
        int dataLength = utils::getDataLength((char*)key, attribute);
        offset += dataLength;
        node.keys.push_back(key);
        node.children.push_back(child);
    }
//    assert(offset != 0);
    if (size !=0) node.children.push_back(*(unsigned *)((char *)page + offset));
    return STD_SUCC;
}

RC IndexManager::getEntries(const void *page, const Attribute &attribute, LeafNode &node) const {
    int size = node.numOfSlots;
    int offset = 0;
    node.entries.clear();
    for (int i = 0; i < size; i++) {
        void* key = (void*)((char*)page + offset);
        offset += utils::getDataLength((char*)page + offset, attribute);
        RID rid;
        rid.pageNum = *(unsigned *)((char*)page + offset);
        rid.slotNum = *(unsigned *) ((char*)page + offset + 4);
        offset += 8;
        node.entries.push_back(make_pair(key, rid));
    }
    return STD_SUCC;
}

bool IndexManager::isLeafNode(IXFileHandle &ixfileHandle, const Attribute &attribute, unsigned pageNum) {
    char page[PAGE_SIZE] = {};
    ixfileHandle._fileHandle->readPage(pageNum, page);
    if (this->getNodeFlag(page) == 1)
        return true;
    return false;
}

IX_ScanIterator::IX_ScanIterator()
{
    this->isOpen = false;
    this->_highKey = new char[PAGE_SIZE] {};
}

IX_ScanIterator::~IX_ScanIterator()
{
    delete[] (char*)this->_highKey;
}

RC IX_ScanIterator::setParams(IXFileHandle &ixfileHandle, const LeafNode& node, const unsigned pos, const Attribute attribute,
             const void *highKey, const bool highKeyInclusive, IndexManager* indexManager)
{
    if(this->isOpen) this->close();
    this->_highkeyIsNull = false;
    this->_ixfileHandle = &ixfileHandle;
    this->_node = node;
    this->_pos = pos;
    this->_attribute = attribute;
    memset(this->_highKey, 0, PAGE_SIZE);
    if(highKey == NULL) this->_highkeyIsNull = true;
    else {
        unsigned dataLength = utils::getDataLength(highKey, attribute);
        memcpy(this->_highKey, highKey, dataLength);
    }
    this->_highKeyInclusive = highKeyInclusive;
    this->_indexManager = indexManager;
    this->isOpen = true;
    return  STD_SUCC;
}

RC IX_ScanIterator::getNextEntry(RID &rid, void *key)
{
    if(!this->isOpen) return SCAN_NOT_OPEN;
    if (this->_pos < this->_node.entries.size())
    {
        if((!this->_highkeyIsNull) and ((this->_highKeyInclusive && utils::compare(this->_attribute, this->_node.entries[this->_pos].first, this->_highKey) > 0) or
            (!this->_highKeyInclusive && utils::compare(this->_attribute, this->_node.entries[this->_pos].first, this->_highKey) >= 0)))
        {
            this->close();
            return IX_EOF;
        }
        memcpy(key, this->_node.entries[this->_pos].first, utils::getDataLength(this->_node.entries[this->_pos].first, this->_attribute));
        rid = this->_node.entries[this->_pos].second;
        this->_pos ++;
        return STD_SUCC;
    }
    if(this->_node.next == 0) {this->close(); return IX_EOF;}
    this->_pos = 0;
    RC rc = this->_indexManager->readNode(*this->_ixfileHandle, this->_attribute, this->_node.next, this->_node.page);
    if(rc != STD_SUCC) return rc;
    rc = this->_indexManager->loadLeafNode(this->_attribute, this->_node, this->_node.next, this->_node.page);
    if(rc != STD_SUCC) return rc;
    return this->getNextEntry(rid, key);
}

RC IX_ScanIterator::close()
{
    this->isOpen = false;
    return STD_SUCC;
}


IXFileHandle::IXFileHandle()
{
    ixReadPageCounter = 0;
    ixWritePageCounter = 0;
    ixAppendPageCounter = 0;
    this->_fileHandle = new FileHandle();
}

IXFileHandle::~IXFileHandle()
{
    this->_fileHandle->eraseBuf();
    this->_fileHandle->~FileHandle();
    delete this->_fileHandle;
}

RC IXFileHandle::collectCounterValues(unsigned &readPageCount, unsigned &writePageCount, unsigned &appendPageCount)
{
    RC rc = this->_fileHandle->collectCounterValues(ixReadPageCounter, ixWritePageCounter, ixAppendPageCounter);
    readPageCount = ixReadPageCounter;
    writePageCount = ixWritePageCounter;
    appendPageCount = ixAppendPageCounter;
    return rc;
}
