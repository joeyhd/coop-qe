
include_directories ("${PROJECT_SOURCE_DIR}/src/cli")

set (CLI_LIBS ${CLI_LIBS} pfm rbfm utils qe cli)

macro (execute CUR_TEST)
    add_executable (${CUR_TEST} "${CUR_TEST}.cc")
    target_link_libraries (${CUR_TEST} ${CLI_LIBS})
endmacro ()

# execute (cli_example_01)
# execute (cli_example_02)
# execute (cli_example_03)
# execute (cli_example_04)
# execute (cli_example_05)
# execute (cli_example_06)
# execute (cli_example_07)
# execute (cli_example_08)
# execute (cli_example_09)
# execute (cli_example_10)
# execute (cli_example_11)
# execute (cli_example_12)