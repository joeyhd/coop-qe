
#include "qe.h"

#include <cassert>
#include <cstring>
#include <string>
#include <iostream>

#include "../rbf/utils.h"
#include "../rbf/rbfm.h"

using namespace std;

Filter::Filter(Iterator* input, const Condition &condition):input(input), condition(condition) {
	assert(!condition.bRhsIsAttr);
    this->getAttributes(attrs);
    oattrs.push_back(condition.lhsAttr);
}

RC Filter::getNextTuple(void *data) {
	Attribute tmpAttr;
	tmpAttr.type = condition.rhsValue.type;
	char tmpdata[PAGE_SIZE] = {};
	RC rc;
	while(true) {
        memset(data, 0, 4);
		rc = input->getNextTuple(data);
		if(rc != STD_SUCC) return rc;
		char view[PAGE_SIZE];
		memcpy(view, data, 100);
		utils::extractAttrFromData(data, attrs, tmpdata, oattrs);
		if(*(char*)tmpdata > 0) continue; // is null
		int cmp = utils::compare(tmpAttr, (void*)((char*)tmpdata+1), condition.rhsValue.data);
		if((condition.op == EQ_OP && cmp == 0) ||
			(condition.op == LT_OP && cmp < 0) ||
			(condition.op == LE_OP && cmp <=0) ||
			(condition.op == GT_OP && cmp > 0) ||
			(condition.op == GE_OP && cmp >=0) ||
			(condition.op == NE_OP && cmp !=0) ||
			(condition.op == NO_OP)) return STD_SUCC;
	}
}

void Filter::getAttributes(vector<Attribute> &attrs) const {
	input->getAttributes(attrs);
}

Project::Project(Iterator* input, const vector<string> &attrNames) {
    _it = input;
    this->attrNames = attrNames;
}

RC Project::getNextTuple(void *data) {
    vector<Attribute> attrs;
    char buf[PAGE_SIZE] = {};
    if (_it->getNextTuple(buf) != RM_EOF) {
        _it->getAttributes(attrs);
        utils::extractAttrFromData(buf, attrs, data, this->attrNames);
        return STD_SUCC;
    } else
        return QE_EOF;
}

void Project::getAttributes(vector<Attribute> &attrs) const {
    vector<Attribute> originalAttrs;
    _it->getAttributes(originalAttrs);
    for (int i = 0; i < originalAttrs.size(); i++) {
        for (int j = 0; j < this->attrNames.size(); j++) {
            if (originalAttrs[i].name == this->attrNames[j]) attrs.push_back(originalAttrs[i]);
        }
    }
}

INLJoin::INLJoin(Iterator *leftIn, IndexScan *rightIn, const Condition &condition) {
    this->_leftIn = leftIn;
    this->_rightIn = rightIn;
    this->_condition = condition;
    this->_leftIn->getAttributes(this->_leftAttrs);
    this->_leftData = new char[PAGE_SIZE] {};
    this->_leftIn->getNextTuple(this->_leftData);
    this->_rightIn->getAttributes(this->_rightAttrs);
    char odata[PAGE_SIZE] = {};
	vector<string> attr;
	attr.push_back(condition.lhsAttr);
    utils::extractAttrFromData(this->_leftData, this->_leftAttrs, odata, attr);
    char *key = odata + 1;
    this->_rightIn->setIterator(key, key, true, true);
}

INLJoin::~INLJoin() {
    delete[] this->_leftData;
}

RC INLJoin::getNextTuple(void *data) {
    char buf[PAGE_SIZE] = {};
    RC rc = this->_rightIn->getNextTuple(buf);
    if (rc == STD_SUCC) {
        this->joinData(this->_leftData, buf, data);
    } else {
        RC rc = this->_leftIn->getNextTuple(this->_leftData);
        if (rc != STD_SUCC) return QE_EOF;
        char key[PAGE_SIZE];
		vector<string> attr;
		attr.push_back(this->_condition.lhsAttr);
        utils::extractAttrFromData(this->_leftData, this->_leftAttrs, key, attr);
        this->_rightIn->setIterator(key + 1, key + 1, true, true);
        return this->getNextTuple(data);
    }
    return STD_SUCC;
}

void INLJoin::getAttributes(vector<Attribute> &attrs) const {
    attrs.insert(attrs.end(), this->_leftAttrs.begin(), this->_leftAttrs.end());
    attrs.insert(attrs.end(), this->_rightAttrs.begin(), this->_rightAttrs.end());
}

RC INLJoin::joinData(void *leftData, void *rightData, void *outputData) {
    const int leftFieldNum = this->_leftAttrs.size();
    const int leftNullByteNum = (leftFieldNum-1)/8+1;
    const int rightFieldNum = this->_rightAttrs.size();
    const int rightNullByteNum = (rightFieldNum-1)/8+1;
    const void *ptr = (void*)((char*)leftData + leftNullByteNum);
    unsigned leftDataLength = 0;
    char view[200];
    memcpy(view, leftData, 200);
    char view2[200];
    memcpy(view2, rightData, 200);
    for (int i = 0; i < leftFieldNum; i++) {
        unsigned length = utils::getDataLength((void*)((char *)ptr + leftDataLength), this->_leftAttrs[i]);
        leftDataLength += length;
    }
    ptr = (void*)((char*)rightData + rightNullByteNum);
    unsigned rightDataLength = 0;
    for (int i = 0; i < rightFieldNum; i++) {
        unsigned length = utils::getDataLength((void*)((char *)ptr + rightDataLength), this->_rightAttrs[i]);
        rightDataLength += length;
    }
    int nullByteNum = (leftFieldNum + rightFieldNum - 1) / 8+1;
    char nullByte[PAGE_SIZE] = {};
    memcpy(nullByte, leftData, leftNullByteNum);
    for (int i = 0; i < rightFieldNum; i++) {
        if (*((byte*)rightData+i/8) & (1 << i%(7 - i))) {
            *(nullByte+(i+leftFieldNum)/8) |= 1 << (7 - (leftFieldNum + i) % 8);
        }
    }
    unsigned offset = 0;
    memcpy(outputData, nullByte, nullByteNum);
    offset = nullByteNum;
    memcpy((char *)outputData + offset, (char *)leftData + leftNullByteNum, leftDataLength);
    offset += leftDataLength;
    memcpy((char *)outputData + offset, (char *)rightData + rightNullByteNum, rightDataLength);
    return STD_SUCC;
}

BNLJoin::BNLJoin(Iterator *leftIn, TableScan *rightIn, const Condition &condition, const unsigned numPages):condition(condition), leftIn(leftIn), rightIn(rightIn) {
	assert(condition.bRhsIsAttr);
	this->pages = new char[PAGE_SIZE*numPages];
	this->numPages = numPages;
	leftIn->getAttributes(lattrs);
	rightIn->getAttributes(rattrs);

	unsigned i;
	for (i = 0; i < lattrs.size(); ++i) {
		if(condition.lhsAttr == lattrs[i].name) {
			this->lattr = lattrs[i];
			break;
		}
	}
	assert(i < lattrs.size());
    for (i = 0; i < rattrs.size(); ++i) {
        if(condition.rhsAttr == rattrs[i].name) {
            this->rattr = rattrs[i];
            break;
        }
    }
	assert(i < lattrs.size());
}

BNLJoin::~BNLJoin() {
	delete[] this->pages;
}

RC BNLJoin::getNextTuple(void *data) {
	RC rc;
	char tmpdata[PAGE_SIZE];
	vector<string> _rattrs;
	_rattrs.push_back(this->rattr.name);
	while(true) {
        if(this->tupleSize == 0) {
            rc = loadNextBlock();
            if(rc != STD_SUCC) return rc;
        }
		memset(data, 0, 4);
		rc = this->rightIn->getNextTuple(data);
		if(rc == QE_EOF) {
			rc = loadNextBlock();
			if(rc != STD_SUCC) return rc;
		}else if(rc != STD_SUCC) return rc;
		utils::extractAttrFromData(data, rattrs, tmpdata, _rattrs);
		map<MyHash, RID>::iterator it = _map.find(MyHash(this->rattr.type, tmpdata+1));
		if(it != _map.end()) {
			RID rid = it->second;
			char* t_page = this->pages + PAGE_SIZE * rid.pageNum;
			void* ldata = t_page + PageManager::getSlotAdd(t_page, rid.slotNum);
			PageManager::concatData(lattrs, ldata, rattrs, data, data);
			return STD_SUCC;
		} else {
			continue;
		}
	}
}

void BNLJoin::getAttributes(vector<Attribute> &attrs) const {
	attrs.clear();
	for(unsigned i = 0; i < lattrs.size(); ++i) {
		attrs.push_back(lattrs[i]);
	}
	for(unsigned i = 0; i < rattrs.size(); ++i) {
		attrs.push_back(rattrs[i]);
	}
}

RC BNLJoin::loadNextBlock() {
	RC rc = STD_SUCC;
	this->tupleSize = 0; this->pos = 0;
	this->_map.clear();
	RelationManager &rm = this->rightIn->rm;
    string tableName = this->rightIn->tableName;
    delete this->rightIn;
    this->rightIn = new TableScan(rm, tableName);
	if(finish) return QE_EOF;
	if(!hasBuf) {
		rc = leftIn->getNextTuple((void*)buf);
		if (rc != STD_SUCC) return rc;
		hasBuf = true;
	}
	vector<string> _lattrs;
	_lattrs.push_back(this->lattr.name);

	RID rid; rid.pageNum = 0; rid.slotNum = 0;
	while(rid.pageNum < this->numPages) {
		char* t_page = pages + PAGE_SIZE * rid.pageNum;
		if(rid.slotNum == 0) {
			PageManager::setPageFreespace(t_page, PAGE_SIZE-4);
			PageManager::setSlotsNum(t_page, 0);
		}
		unsigned dataLength = PageManager::getDataLength(lattrs, buf);
		unsigned freespace = PageManager::getPageFreespace(t_page);
		if(freespace-4 < dataLength) {
			rid.pageNum ++;
			rid.slotNum = 0;
			continue;
		}
		unsigned slotNum = PageManager::getSlotsNum(t_page);
		unsigned add = 0;
		if(slotNum > 0) {
			add = PageManager::getSlotAdd(t_page, slotNum-1) + PageManager::getSlotLen(t_page, slotNum-1);
		}
		memcpy(t_page + add, buf, dataLength);
        PageManager::setPageFreespace(t_page, freespace-4-dataLength);
        PageManager::setSlotsNum(t_page, slotNum+1);
        PageManager::setSlotAdd(t_page, rid.slotNum, add);
        PageManager::setSlotLen(t_page, rid.slotNum, dataLength);

		char tmp[PAGE_SIZE] = {};
		utils::extractAttrFromData(t_page + add, lattrs, tmp, _lattrs);
		_map.insert(pair<MyHash, RID>(MyHash(this->lattr.type, tmp+1), rid));
		this->tupleSize ++;
		rc = leftIn->getNextTuple((void*)buf);
		if (rc == QE_EOF) {
			this->finish = true;
			break;
		}
		if (rc != STD_SUCC) return rc;
		rid.slotNum ++;
	}
	return STD_SUCC;
}

Aggregate::Aggregate(Iterator *input, Attribute aggAttr, AggregateOp op): _input(input) {
	this->_aggAttr = aggAttr;
	this->_op = op;
    this->isGroupAggregate = false;
	this->finished = false;
	this->_input->getAttributes(_attrs);

	char buf[PAGE_SIZE] = {};
	float result = 0;

	unsigned cnt = 0;
	while (this->_input->getNextTuple(buf) == STD_SUCC) {
		vector<string> attr;
		char value[PAGE_SIZE] = {};
		attr.push_back(this->_aggAttr.name);
		utils::extractAttrFromData(buf, _attrs, value, attr);
		float tmp;
		switch (this->_aggAttr.type) {
			case TypeInt:
				tmp = *(int *)(value + 1);
				break;
			case TypeReal:
				tmp = *(float *)(value + 1);
				break;
			default:
				tmp = 0;
		}
		result = this->_OP(result, tmp, cnt);
	}
	this->_result = result;
}

Aggregate::Aggregate(Iterator *input, Attribute aggAttr, Attribute groupAttr, AggregateOp op): _input(input) {
	this->_aggAttr = aggAttr;
	this->_groupAttr = groupAttr;
	this->_op = op;
    this->isGroupAggregate = true;
    this->finished = false;
	input->getAttributes(_attrs);

	char buf[PAGE_SIZE] = {};
	while(this->_input->getNextTuple(buf) == STD_SUCC) {
		vector<string> groupAttr, attr;
		char groupValue[PAGE_SIZE] = {};
		char value[PAGE_SIZE] = {};
		groupAttr.push_back(this->_groupAttr.name);
		attr.push_back(this->_aggAttr.name);
		utils::extractAttrFromData(buf, _attrs, groupValue, groupAttr);
		utils::extractAttrFromData(buf, _attrs, value, attr);

		MyHash myHash = MyHash(this->_groupAttr.type, groupValue+1);
		map<MyHash, gdata>::iterator it = _map.find(myHash);
		
		float tmp;
		switch (this->_aggAttr.type) {
			case TypeInt:
				tmp = *(int *)(value + 1);
				break;
			case TypeReal:
				tmp = *(float *)(value + 1);
				break;
			default:
				tmp = 0;
		}

		if(it != _map.end()) {
			it->second.result = this->_OP(it->second.result, tmp, it->second.cnt);
		} else {
			gdata tmpgdata;
			memcpy(tmpgdata.data, groupValue, 1+utils::getDataLength(groupValue+1, this->_groupAttr));
			tmpgdata.cnt = 0;
			tmpgdata.result = this->_OP(0, tmp, tmpgdata.cnt);
			_map.insert(pair<MyHash, gdata>(myHash, tmpgdata));
		}
	}
	this->_ite = _map.begin();
}

RC Aggregate::getNextTuple(void *data) {
    if(isGroupAggregate) {
    	if(this->_ite == _map.end()) {
    		_map.clear();
    		return QE_EOF;
    	}
    	unsigned dataLength = utils::getDataLength(_ite->second.data+1, this->_aggAttr);
    	memcpy(data, _ite->second.data, 1+dataLength);
    	*(float*)((char*)data + dataLength + 1) = _ite->second.result;
    	_ite ++;
    	return STD_SUCC;

    } else {
   		if (this->finished) return QE_EOF;
		memset(data, 0, 1);
		*(float*)((char*)data+1) = this->_result;
		this->finished = true;
		return STD_SUCC;
    }
}



float Aggregate::_OP(const float oriRes, const float curVal, unsigned& cnt) {
	float ret;
	switch (this->_op) {
		case MAX:
			ret = (cnt == 0) ? curVal : max(oriRes, curVal);
			break;
		case MIN:
			ret = (cnt == 0) ? curVal : min(oriRes, curVal);
			break;
		case AVG:
			ret = (oriRes*cnt + curVal)/(cnt+1);
			break;
		case SUM:
			ret = oriRes + curVal;
			break;
		case COUNT:
			ret = oriRes + 1;
			break;
	}
	cnt ++;
	return ret;
}

void Aggregate::getAttributes(vector<Attribute> &attrs) const {
    Attribute attr;
    string attrName = this->_aggAttr.name;
    switch (this->_op) {
        case MAX:
            attrName = "MAX(" + attrName + ")";
            break;
        case MIN:
            attrName = "MIN(" + attrName + ")";
            break;
        case AVG:
            attrName = "AVG(" + attrName + ")";
            break;
        case SUM:
            attrName = "SUM(" + attrName + ")";
            break;
        default:
            attrName = "COUNT(" + attrName + ")";
    }
    attr.name = attrName;
    attr.type = TypeReal;
    attr.length = 4;
    attrs.push_back(attr);
}

MyHash::MyHash(AttrType type, void* data) {
	this->type = type;
	if (type == TypeInt) {
		hash<int> ptr_hash;
		hashVal = ptr_hash(*(int*)data);
	} else if (type == TypeReal) {
		hash<float> ptr_hash;
		hashVal = ptr_hash(*(float*)data);
	} else {
		hash<string> ptr_hash;
		char tmp[PAGE_SIZE] = {};
		memcpy(tmp, (char*)data+4, *(int*)data);
		hashVal = ptr_hash(string(tmp));
	}
}

bool operator==(const MyHash& a, const MyHash& b) {
	return (a.type == b.type && a.hashVal == b.hashVal);
}

bool operator<(const MyHash& a, const MyHash& b) {
	return (a.hashVal < b.hashVal);
}

unsigned PageManager::getPageFreespace(const void *page)
{ return(PageManager::getSlotInfo(page, 0)); }

unsigned PageManager::getSlotsNum(const void *page)
{ return(PageManager::getSlotInfo(page, 1)); }

unsigned PageManager::getSlotAdd(const void *page, unsigned slotIndex)
{ return(PageManager::getSlotInfo(page, slotIndex*2+2)); }

unsigned PageManager::getSlotLen(const void *page, unsigned slotIndex)
{ return(PageManager::getSlotInfo(page, slotIndex*2+3)); }

unsigned PageManager::getSlotInfo(const void *data, unsigned index)
{ return utils::getShortIntAt(data, PAGE_SIZE-2*index-2); }

void PageManager::setPageFreespace(const void *page, const unsigned num)
{ PageManager::setSlotInfo(page, 0, num); }

void PageManager::setSlotsNum(const void *page, const unsigned num)
{ PageManager::setSlotInfo(page, 1, num); }

void PageManager::setSlotAdd(const void *page, unsigned slotIndex, const unsigned num)
{ PageManager::setSlotInfo(page, slotIndex*2+2, num); }

void PageManager::setSlotLen(const void *page, unsigned slotIndex, const unsigned num)
{ PageManager::setSlotInfo(page, slotIndex*2+3, num); }

void PageManager::setSlotInfo(const void *data, const unsigned index, const unsigned num)
{ utils::setShortIntAt(data, PAGE_SIZE-2*index-2, num); }

unsigned PageManager::getDataLength(const vector<Attribute> &attrs, const void *data) {
	const int fieldNum = attrs.size();
    const int nullByteNum = (fieldNum-1)/8+1;
    const void *ptr = (void*)((char*)data + nullByteNum);
    for (int i = 0; i < fieldNum; i++) {
        if (*((byte*)data+i/8) & (128 >> i%8)) continue;
        if (attrs[i].type == TypeInt) ptr = (void*)((char*)ptr + 4);
        else if (attrs[i].type == TypeReal) ptr = (void*)((char*)ptr + 4);
        else ptr = (void*)((char*)ptr + 4 + *((int*)ptr));
    }
    return unsigned((char*)ptr - (char*)data);
}

void PageManager::concatData(const vector<Attribute> &lattrs, const void* ldata, const vector<Attribute> &rattrs, const void* rdata, void* odata) {
	const int lfieldNum = lattrs.size(), rfieldNum = rattrs.size();
    const int lnullByteNum = (lfieldNum-1)/8+1, rnullByteNum = (rfieldNum-1)/8+1;
    const void *lptr = (void*)((char*)ldata + lnullByteNum), *rptr = (void*)((char*)rdata + rnullByteNum);
	char tmp[PAGE_SIZE] = {};
    int fieldNum = lfieldNum + rfieldNum;
    int nullByteNum = (fieldNum-1)/8+1;
    for(int i = 0; i < lfieldNum; i ++) {
    	*(tmp + i/8) &= (128 >> i%8);
    }
    for(int i = 0; i < rfieldNum; i ++) {
    	*(tmp + (i+lfieldNum)/8) &= (128 >> (i+lfieldNum)%8);
    }
    unsigned lpureDataLen = PageManager::getDataLength(lattrs, ldata)-lnullByteNum;
    unsigned rpureDataLen = PageManager::getDataLength(rattrs, rdata)-rnullByteNum;
    memcpy(tmp+nullByteNum, (char*)ldata+lnullByteNum, lpureDataLen);
    memcpy(tmp+nullByteNum+lpureDataLen, (char*)rdata+rnullByteNum, rpureDataLen);
    memcpy(odata, tmp, nullByteNum + lpureDataLen + rpureDataLen);

}

GHJoin::GHJoin(Iterator *leftIn, Iterator *rightIn, const Condition &condition, const unsigned numPartitions):leftIn(leftIn), rightIn(rightIn) {
	assert(condition.bRhsIsAttr);
	this->numPartitions = numPartitions;
	this->lfileNames.resize(numPartitions);
	this->rfileNames.resize(numPartitions);

	leftIn->getAttributes(lattrs);
	rightIn->getAttributes(rattrs);

	unsigned i;
	for (i = 0; i < lattrs.size(); ++i) {
		if(condition.lhsAttr == lattrs[i].name) {
			this->lattr = lattrs[i];
			break;
		}
	}
	assert(i < lattrs.size());
    for (i = 0; i < rattrs.size(); ++i) {
        if(condition.rhsAttr == rattrs[i].name) {
            this->rattr = rattrs[i];
            break;
        }
    }
    cout << i << " " << rattrs.size() << endl;
	assert(i < rattrs.size());
	this->partition();
}

GHJoin::~GHJoin() {
	for (int i = 0; i < numPartitions; ++i) {
		remove(lfileNames[i].c_str()); remove(rfileNames[i].c_str());
	}
	for (int i = 0; i < this->pages.size(); ++i)
	{
		delete[] this->pages[i];
	}
	this->pages.clear();
}

RC GHJoin::getNextTuple(void *data) {
	RC rc;
	if(curPartition == 0) {
		rc = loadNextPartition();
		if(rc != STD_SUCC) return rc;
	}
	RBFM_ScanIterator *scan;
	vector<Attribute> *attrs;
	Attribute *attr;
	if(!this->buildLeft) {
		scan = &lscan;
		attr = &lattr;
		attrs = &lattrs;
	} else {
		scan = &rscan;
		attr = &rattr;
		attrs = &rattrs;
	}
	vector<string> _attr;
	_attr.push_back(attr->name);

	char tmpdata[PAGE_SIZE];
	memset(data, 0, 4);

	while(true) {
		rc = scan->getNextRecord(_rid, data);
		if(rc == QE_EOF) {
			if( curPartition == numPartitions) return QE_EOF;
			rc = loadNextPartition();
			if(rc != STD_SUCC) return rc;
			continue;
		}
		if(rc != STD_SUCC) return rc;
		utils::extractAttrFromData(data, *attrs, tmpdata, _attr);
		map<MyHash, RID>::iterator it = _map.find(MyHash(attr->type, tmpdata+1));
		if(it != _map.end()) {
			RID rid = it->second;
			char* t_page = this->pages[rid.pageNum];
			void* hdata = t_page + PageManager::getSlotAdd(t_page, rid.slotNum);
			if(buildLeft) PageManager::concatData(lattrs, hdata, rattrs, data, data);
			else PageManager::concatData(lattrs, data, rattrs, hdata, data);

			return STD_SUCC;
		} 
		memset(data, 0, 4);
	}
}

void GHJoin::getAttributes(vector<Attribute> &attrs) const {
	attrs.clear();
	for(unsigned i = 0; i < lattrs.size(); ++i) {
		attrs.push_back(lattrs[i]);
	}
	for(unsigned i = 0; i < rattrs.size(); ++i) {
		attrs.push_back(rattrs[i]);
	}
}

RC GHJoin::loadNextPartition() {
	if(lopen) { lrbfm->closeFile(lfh); lopen = false;}
	if(ropen) { rrbfm->closeFile(rfh); ropen = false;}
	RC rc = lrbfm->openFile(lfileNames[curPartition], lfh);
	if(rc != STD_SUCC) return rc;
	lopen = true;
	rc = rrbfm->openFile(rfileNames[curPartition], rfh);
	if(rc != STD_SUCC) return rc;
	ropen = true;
	vector<string> _lattrs;
	for(int i = 0; i < lattrs.size(); i++) _lattrs.push_back(lattrs[i].name);
	rc = lscan.setParams(lfh, lattrs, lattr.name, NO_OP, NULL, _lattrs, lrbfm);
	if(rc != STD_SUCC) return rc;
	vector<string> _rattrs;
	for(int i = 0; i < rattrs.size(); i++) _rattrs.push_back(rattrs[i].name);
	rc = rscan.setParams(rfh, rattrs, rattr.name, NO_OP, NULL, _rattrs, rrbfm);
	if(rc != STD_SUCC) return rc;

	this->_map.clear();
	_rid.slotNum = 0; _rid.pageNum = 0;

	RBFM_ScanIterator *scan;
	vector<Attribute> *attrs;
	Attribute *attr;
	Iterator *ite;
	if(this->buildLeft) {
		scan = &lscan;
		attrs = &lattrs;
		attr = &lattr;
		ite = leftIn;
	} else {
		scan = &rscan;
		attrs = &rattrs;
		attr = &rattr;
		ite = rightIn;
	}
	vector<string> _attr;
	_attr.push_back(attr->name);

	char buf[PAGE_SIZE] = {};
	RID rid; rid.pageNum = 0; rid.slotNum = 0;
	RID tmprid;
	rc = scan->getNextRecord(tmprid, buf);
	while(rc == STD_SUCC) {
		if(pages.size() == rid.pageNum) {
			char* page = new char[PAGE_SIZE] {};
			pages.push_back(page);
		}
		char* t_page = pages[rid.pageNum];
		if(rid.slotNum == 0) {
            PageManager::setPageFreespace(t_page, PAGE_SIZE - 4);
            PageManager::setSlotsNum(t_page, 0);
        }
		unsigned dataLength = PageManager::getDataLength(*attrs, buf);
		unsigned freespace = PageManager::getPageFreespace(t_page);
		if(freespace-4 < dataLength) {
			rid.pageNum ++;
			rid.slotNum = 0;
			continue;
		}
		unsigned slotNum = PageManager::getSlotsNum(t_page);
		unsigned add = 0;
		if(slotNum > 0) {
			add = PageManager::getSlotAdd(t_page, slotNum-1) + PageManager::getSlotLen(t_page, slotNum-1);
		}
		memcpy(t_page + add, buf, dataLength);
		PageManager::setPageFreespace(t_page, freespace-4-dataLength);
        PageManager::setSlotsNum(t_page, slotNum+1);
		PageManager::setSlotAdd(t_page, rid.slotNum, add);
		PageManager::setSlotLen(t_page, rid.slotNum, dataLength);
		
		char tmp[PAGE_SIZE] = {};
		utils::extractAttrFromData(buf, *attrs, tmp, _attr);
		_map.insert(pair<MyHash, RID>(MyHash(attr->type, tmp+1), rid));
        unsigned size = _map.size();
		rid.slotNum ++;

		rc = scan->getNextRecord(tmprid, buf);
	}

    curPartition ++;
	return STD_SUCC;
}

RC GHJoin::partition() {
	RecordBasedFileManager *rbfm = RecordBasedFileManager::instance();
	FileHandle fileHandle;

	int _lcnt[10] = {0};
	char name_identy[2] = "A";
	while(true) {
		string tmpstr = "";
		tmpstr += name_identy;
        tmpstr += ".l" + to_string(0) + ".partition";
		if(!utils::FileExists(tmpstr))break;
		name_identy[0] ++;
	}
	for (int i = 0; i < numPartitions; ++i) {
		lfileNames[i] += name_identy;
        lfileNames[i] += ".l" + to_string(i) + ".partition";
		rfileNames[i] += name_identy;
        rfileNames[i] += ".r" + to_string(i) + ".partition";
		rbfm->createFile(lfileNames[i]); rbfm->createFile(rfileNames[i]);
	}

	vector<string> _lattr; _lattr.push_back(lattr.name);
	vector<string> _rattr; _rattr.push_back(rattr.name);

	char data[PAGE_SIZE];
	RID rid; RC rc;
	unsigned lcnt = 0 , rcnt = 0;
	while(leftIn->getNextTuple(data) == STD_SUCC) {
		lcnt ++;
		char tmp[PAGE_SIZE];
		utils::extractAttrFromData(data, lattrs, tmp, _lattr);
		size_t hashVal = MyHash(this->lattr.type, tmp+1).hashVal;
		_lcnt[hashVal%numPartitions] ++;
		rc = rbfm->openFile(lfileNames[hashVal%numPartitions], fileHandle);
		if(rc != STD_SUCC) return rc;
		rc = rbfm->insertRecord(fileHandle, lattrs, data, rid);
		if(rc != STD_SUCC) return rc;
		rc = rbfm->closeFile(fileHandle);
		if(rc != STD_SUCC) return rc;
	}
	while(rightIn->getNextTuple(data) == STD_SUCC) {
		rcnt ++;
		char tmp[PAGE_SIZE];
		utils::extractAttrFromData(data, rattrs, tmp, _rattr);
		size_t hashVal = MyHash(this->rattr.type, tmp+1).hashVal;
		rc = rbfm->openFile(rfileNames[hashVal%numPartitions], fileHandle);
		if(rc != STD_SUCC) return rc;
		rc = rbfm->insertRecord(fileHandle, rattrs, data, rid);
		if(rc != STD_SUCC) return rc;
		rc = rbfm->closeFile(fileHandle);
		if(rc != STD_SUCC) return rc;
	}
	this->buildLeft = lcnt < rcnt;
	return STD_SUCC;
}