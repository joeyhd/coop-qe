#ifndef SRC_RBF_UTILS_H_
#define SRC_RBF_UTILS_H_

#include <iostream>
#include <string>
#include <fstream>
#include "pfm.h"
#include "rbfm.h"

using namespace std;

enum RET{STD_SUCC=0, STD_ERR, FILE_EXISTS, FILE_NOT_EXISTS, // 0,1,2,3
        FILE_NOT_CLOSE, FILE_NOT_OPEN, ACCESS_OUT_BOUND,  // 4,5,6
        FILE_STREAM_BROKEN, ATTRIBUTE_NOT_FOUND, // 7,8
        SCAN_NOT_OPEN, POINTED_PAGE_FULL, PERMISSION_DENIED, // 9,10,11
        NOT_AUTHORIZED, NON_EXIST_ENTRY}; // 12,13

typedef struct {bool insd;} insflag;
const insflag INSD = {true};
typedef struct {bool nb;} data_null;
const data_null DATANULL = {true};

class utils
{
public:
    utils(){};
    ~utils(){};
    static bool FileExists(const string &fileName);
    static void setShortIntAt(const void *data, const unsigned index, const unsigned num);
    static unsigned getShortIntAt(const void *data, const unsigned index);
    static void read(istream& is, void* value);
    static void read(istream& is, void* value, const unsigned size, const unsigned offset=0);
    static void write(ostream& os, const void* value, const unsigned size, const unsigned offset=0);
    static Attribute setAttribute(string name, AttrType type, AttrLength length);
    static Attribute convertToAttribute(char* data);
    static int compare(const Attribute &attribute, const void* a, const void* b);
    static unsigned getDataLength(const void *data, const Attribute &attribute);
    static void printPage(char* page, unsigned size);
    static void extractAttrFromData(const void* slot, const vector<Attribute>& iattrs,
          void* data, const vector<string>& oattrs);
};

class dataGenerator
{
public:
    dataGenerator();
    ~dataGenerator();
    dataGenerator & operator<<(const int);
    dataGenerator & operator<<(const unsigned);
    dataGenerator & operator<<(const float);
    dataGenerator & operator<<(const double);
    dataGenerator & operator<<(const string);
    dataGenerator & operator<<(const data_null);
    RID operator<<(const insflag);
    void setInserter(RecordBasedFileManager* rbfm, FileHandle* fh, vector<Attribute>& _descriptor);
    void reset();
private:
    unsigned _curpos, _nullpos;
    char _data[PAGE_SIZE];
    char _puredata[PAGE_SIZE];
    RecordBasedFileManager* _rbfm;
    FileHandle* _fh;
    vector<Attribute> _descriptor;
};

#endif
