#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cassert>
#include "pfm.h"
#include "rbfm.h"
#include "utils.h"

using namespace std;

RecordBasedFileManager* RecordBasedFileManager::_rbf_manager = 0;

RecordBasedFileManager* RecordBasedFileManager::instance()
{
    if(!_rbf_manager)
        _rbf_manager = new RecordBasedFileManager();

    return _rbf_manager;
}

RecordBasedFileManager::RecordBasedFileManager()
{
    this->pfm = PagedFileManager::instance();
}

RecordBasedFileManager::~RecordBasedFileManager()
{
}

RC RecordBasedFileManager::createFile(const string &fileName) {
    return this->pfm->createFile(fileName);
}

RC RecordBasedFileManager::destroyFile(const string &fileName) {
    return this->pfm->destroyFile(fileName);
}

RC RecordBasedFileManager::openFile(const string &fileName, FileHandle &fileHandle) {
    return this->pfm->openFile(fileName, fileHandle);
}

RC RecordBasedFileManager::closeFile(FileHandle &fileHandle) {
    return this->pfm->closeFile(fileHandle);
}

RC RecordBasedFileManager::insertRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, RID &rid) {
    const unsigned dataLen = this->getDataLength(recordDescriptor, data);
    char catalog[CATALOGBYTE_SIZE] = {};
    return this->insertRecordWithCatalog(fileHandle, recordDescriptor, data, rid, catalog, dataLen);
}

RC RecordBasedFileManager::insertRecordWithCatalog(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor,
                const void *data, RID &rid, const void *catalog, unsigned dataLen) {
    RC rc = findAvaiPage(fileHandle, dataLen + CATALOGBYTE_SIZE, rid.pageNum);
    if(rc != STD_SUCC) return rc;
    rc = insertSlot(fileHandle, rid, data, catalog, dataLen);
    return rc;
}

RC RecordBasedFileManager::updateRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, const RID &rid) {
    const unsigned dataLen = this->getDataLength(recordDescriptor, data);
    char page[PAGE_SIZE];
    fileHandle.readPage(rid.pageNum, page);
    int insertAdd = this->getSlotAdd(page, rid.slotNum);
    int length = this->getSlotLen(page, rid.slotNum);
    int freeSpace = this->getPageFreespace(page);
    int numOfSlots = this->getSlotsNum(page);

    if (length == 0) return STD_ERR;
    if (this->isPointer(rid.slotNum, page)) {
        unsigned short slotNum;
        unsigned pageNum;
        slotNum = *(unsigned short *)(page + insertAdd + CATALOGBYTE_SIZE);
        pageNum = *(unsigned*)(page + insertAdd + CATALOGBYTE_SIZE + SLOTBYTE_SIZE);
        RID newRid;
        newRid.slotNum = slotNum;
        newRid.pageNum = pageNum;
        // if pointer is no longer needed
        if (freeSpace + SLOTBYTE_SIZE + PAGENUMBYTE_SIZE >= dataLen) {
            deleteRecord(fileHandle, recordDescriptor, newRid);
            int shiftLength = PAGE_SIZE - 4 - insertAdd - length - freeSpace - numOfSlots * 4;
            this->shiftData(page, insertAdd, CATALOGBYTE_SIZE + dataLen, length, shiftLength, numOfSlots);
            unsigned char catalogbyte = 0;
            memcpy(page + insertAdd, &catalogbyte, CATALOGBYTE_SIZE);
            memcpy(page + insertAdd + CATALOGBYTE_SIZE, data, dataLen);
            this->setSlotLen(page, rid.slotNum, dataLen + CATALOGBYTE_SIZE);
            this->setPageFreespace(page, freeSpace + SLOTBYTE_SIZE + PAGENUMBYTE_SIZE - dataLen);
            fileHandle.writePage(rid.pageNum, page);
            fileHandle.updateIndexData(rid.pageNum, this->getPageFreespace(page));
            return STD_SUCC;
        }
        RC rc;
        rc = updateRecord(fileHandle, recordDescriptor, data, newRid);
        if (rc == POINTED_PAGE_FULL) {
            deleteRecord(fileHandle, recordDescriptor, newRid);
            char pointedCataLog[CATALOGBYTE_SIZE] = {64};
            insertRecordWithCatalog(fileHandle, recordDescriptor, data, newRid, pointedCataLog, dataLen);
            memcpy(page + insertAdd + CATALOGBYTE_SIZE, &newRid.slotNum, SLOTBYTE_SIZE);
            memcpy(page + insertAdd + CATALOGBYTE_SIZE + SLOTBYTE_SIZE, &newRid.pageNum, PAGENUMBYTE_SIZE);
        }
        return STD_SUCC;
    }

    if ((int) (freeSpace - (dataLen - (length - CATALOGBYTE_SIZE))) < 0) {
        // if current page has no space, insert into a new page and keep a pointer at the current slot
        // assume that current page has enough space for a pointer (7 bytes)
        // if current record is pointed, delete first then insert at other page
        if (this->isPointed(rid.slotNum, page)) return POINTED_PAGE_FULL;

        RID newRid;
        int shiftLength = PAGE_SIZE - 4 - insertAdd - length - freeSpace - numOfSlots * 4;
        char pointedCataLog[CATALOGBYTE_SIZE] = {64};
        insertRecordWithCatalog(fileHandle, recordDescriptor, data, newRid, pointedCataLog, dataLen);
        // read original rid page
        this->shiftData(page, insertAdd,  SLOTBYTE_SIZE + PAGENUMBYTE_SIZE + CATALOGBYTE_SIZE, length, shiftLength, numOfSlots);
        unsigned char pointerCatalog[CATALOGBYTE_SIZE + SLOTBYTE_SIZE + PAGENUMBYTE_SIZE];
        pointerCatalog[0] = 128;
        memcpy(pointerCatalog + 1, &newRid.slotNum, 2);
        memcpy(pointerCatalog + 3, &newRid.pageNum, 4);
        memcpy(page + insertAdd, pointerCatalog, CATALOGBYTE_SIZE + SLOTBYTE_SIZE + PAGENUMBYTE_SIZE);
        this->setSlotLen(page, rid.slotNum, CATALOGBYTE_SIZE + SLOTBYTE_SIZE + PAGENUMBYTE_SIZE);
        this->setPageFreespace(page, freeSpace + length - CATALOGBYTE_SIZE - SLOTBYTE_SIZE - PAGENUMBYTE_SIZE);
        fileHandle.writePage(rid.pageNum, page);
        fileHandle.updateIndexData(rid.pageNum, this->getPageFreespace(page));
    } else {
        int shiftLength = PAGE_SIZE - 4 - insertAdd - length - freeSpace - numOfSlots * 4;
        this->shiftData(page, insertAdd, dataLen + CATALOGBYTE_SIZE, length, shiftLength, numOfSlots);
        memcpy(&page[insertAdd + CATALOGBYTE_SIZE], data, dataLen);
        this->setSlotLen(page, rid.slotNum, dataLen + CATALOGBYTE_SIZE);
        this->setPageFreespace(page, freeSpace + length - dataLen - CATALOGBYTE_SIZE);
        fileHandle.writePage(rid.pageNum, page);
        fileHandle.updateIndexData(rid.pageNum, this->getPageFreespace(page));
    }
    return STD_SUCC;
}

RC RecordBasedFileManager::deleteRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid) {
    char page[PAGE_SIZE];
    RC rc = fileHandle.readPage(rid.pageNum, page);
    if(rc != STD_SUCC) return rc;
    int recordAdd = this->getSlotAdd(page, rid.slotNum);
    int recordLength = this->getSlotLen(page, rid.slotNum);
    int freeSpace = this->getPageFreespace(page);
    int numOfSlots = this->getSlotsNum(page);
    int shiftLength = PAGE_SIZE - 4 - recordAdd - recordLength - freeSpace - numOfSlots * 4;
    // can't delete if slot is deleted
    if (recordLength == 0) return STD_ERR;
    if (this->isPointer(rid.slotNum, page)) {
        unsigned short slotNum;
        unsigned pageNum;
        slotNum = *(unsigned short *)(page + recordAdd + CATALOGBYTE_SIZE);
        pageNum = *(unsigned*)(page + recordAdd + CATALOGBYTE_SIZE + SLOTBYTE_SIZE);
        RID newRid;
        newRid.slotNum  = slotNum;
        newRid.pageNum = pageNum;
        this->deleteRecord(fileHandle, recordDescriptor, newRid);
    }
    memmove(page + recordAdd, page + recordAdd + recordLength, shiftLength); // shift data forward
    // update address of records after the deleted record
    for (int i = rid.slotNum; i < numOfSlots; i++) {
        int address = this->getSlotAdd(page, i);
        if (address > recordAdd) {
            this->setSlotAdd(page, i, address - recordLength);
        }
    }
    // update free space and set length at deleted slot to 0
    this->setSlotLen(page, rid.slotNum, 0);
    this->setPageFreespace(page, freeSpace + recordLength);
    fileHandle.writePage(rid.pageNum, page);
    fileHandle.updateIndexData(rid.pageNum, this->getPageFreespace(page));
    return STD_SUCC;
}

RC RecordBasedFileManager::readRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, void *data) {
    char page[PAGE_SIZE];
    RC rc = fileHandle.readPage(rid.pageNum, page);
    if (rc != STD_SUCC) return rc;
    unsigned insertAdd = this->getSlotAdd(page, rid.slotNum);
    unsigned length = this->getSlotLen(page, rid.slotNum);
    if (length == 0) return STD_ERR;
    if (!this->isPointer(rid.slotNum, page)) {
        memcpy(data, page + insertAdd + CATALOGBYTE_SIZE, length - CATALOGBYTE_SIZE);
    } else {
        RID newRid;
        newRid.slotNum  = utils::getShortIntAt(page, insertAdd + CATALOGBYTE_SIZE);
        newRid.pageNum = *(unsigned*)(page + insertAdd + CATALOGBYTE_SIZE + SLOTBYTE_SIZE);
        return this->readRecord(fileHandle, recordDescriptor, newRid, data);
    }
    return STD_SUCC;
}

RC RecordBasedFileManager::printRecord(const vector<Attribute> &recordDescriptor, const void *data) {
    const int fieldNum = recordDescriptor.size();
    const int nullByteNum = (fieldNum-1)/8+1;
    const void *ptr = (void*)((char*)data + nullByteNum);
    char buf[4096];
    for (int i = 0; i < fieldNum; i++)
    {
        if (*((byte*)data+i/8) & (128 >> i%8))
        {
            cout << recordDescriptor[i].name << " : NULL; " << endl;
            continue;
        }
        cout << recordDescriptor[i].name << " : ";
        if (recordDescriptor[i].type == TypeInt)
        {
            cout << *(int*)ptr << "; ";
            ptr = (void*)((char*)ptr + 4);
        }
        else if (recordDescriptor[i].type == TypeReal)
        {
            cout << *(float*)ptr << "; ";
            ptr = (void*)((char*)ptr + 4);
        }
        else
        {
            strncpy(buf, (char*)ptr+4, *((int*)ptr));
            buf[*((int*)ptr)] = '\0';
            cout << buf << "; ";
            ptr = (void*)((char*)ptr + 4 + *((int*)ptr));
        }
        cout << endl;
    }
    return STD_SUCC;
}

void RecordBasedFileManager::printOne(const Attribute record, const void *ptr)
{
    char buf[4096];
    if (record.type == TypeInt)
    {
        cout << *(int*)ptr << "; " << endl;
    }
    else if (record.type == TypeReal)
    {
        cout << *(float*)ptr << "; " << endl;
    }
    else
    {
        strncpy(buf, (char*)ptr+4, *((int*)ptr));
        buf[*((int*)ptr)] = '\0';
        cout << buf << "; " << endl;
    }
}

RC RecordBasedFileManager::readAttribute(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, const string &attributeName, void *data)
{
    char slot[PAGE_SIZE];
    RC rc = this->readRecord(fileHandle, recordDescriptor, rid, slot);
    if(rc != STD_SUCC) return rc;
    return this->getAttributeFromSlot(recordDescriptor, attributeName, slot, data);
}

unsigned RecordBasedFileManager::getDataLength(const vector<Attribute> &recordDescriptor, const void *data)
{
    const int fieldNum = recordDescriptor.size();
    const int nullByteNum = (fieldNum-1)/8+1;
    const void *ptr = (void*)((char*)data + nullByteNum);
    for (int i = 0; i < fieldNum; i++)
    {
        if (*((byte*)data+i/8) & (128 >> i%8)) continue;
        if (recordDescriptor[i].type == TypeInt) ptr = (void*)((char*)ptr + 4);
        else if (recordDescriptor[i].type == TypeReal) ptr = (void*)((char*)ptr + 4);
        else ptr = (void*)((char*)ptr + 4 + *((int*)ptr));
    }
    return unsigned((char*)ptr - (char*)data);
}

unsigned RecordBasedFileManager::getPageFreespace(const void *page)
{ return(this->getSlotInfo(page, 0)); }

unsigned RecordBasedFileManager::getSlotsNum(const void *page)
{ return(this->getSlotInfo(page, 1)); }

unsigned RecordBasedFileManager::getSlotAdd(const void *page, unsigned slotIndex)
{ return(this->getSlotInfo(page, slotIndex*2+2)); }

unsigned RecordBasedFileManager::getSlotLen(const void *page, unsigned slotIndex)
{ return(this->getSlotInfo(page, slotIndex*2+3)); }

unsigned RecordBasedFileManager::getSlotInfo(const void *data, unsigned index)
{
    // assert index <= 1024
    return utils::getShortIntAt(data, PAGE_SIZE-2*index-2);
}

void RecordBasedFileManager::setPageFreespace(const void *page, const unsigned num)
{ this->setSlotInfo(page, 0, num); }

void RecordBasedFileManager::setSlotsNum(const void *page, const unsigned num)
{ this->setSlotInfo(page, 1, num); }

void RecordBasedFileManager::setSlotAdd(const void *page, unsigned slotIndex, const unsigned num)
{ this->setSlotInfo(page, slotIndex*2+2, num); }

void RecordBasedFileManager::setSlotLen(const void *page, unsigned slotIndex, const unsigned num)
{ this->setSlotInfo(page, slotIndex*2+3, num); }

void RecordBasedFileManager::setSlotInfo(const void *data, const unsigned index, const unsigned num)
{
    utils::setShortIntAt(data, PAGE_SIZE-2*index-2, num);
}

RC RecordBasedFileManager::findAvaiPage(FileHandle &fileHandle, const unsigned dataLen, unsigned &pageNum)
{
    RC rc;
    int t_pageIndex = 0;
    rc = fileHandle.getFirstBigPage(dataLen+4, t_pageIndex);
    if (rc != STD_SUCC) return rc;
    if (t_pageIndex >= 0)
    {
        pageNum = t_pageIndex;
        return rc;
    }
    char page[PAGE_SIZE] = {};
    this->setPageFreespace(page, PAGE_SIZE-4);
    fileHandle.appendPage(page);
    pageNum = fileHandle.getNumberOfDataPages()-1;
    return STD_SUCC;
}

RC RecordBasedFileManager::insertSlot(FileHandle &fileHandle, RID &rid, const void *data, const void *catalog, const unsigned dataLen)
{
    unsigned insertAdd = 0, shiftLength = 0;
    const unsigned insertDataLen = dataLen + CATALOGBYTE_SIZE;
    char page[PAGE_SIZE];

    fileHandle.readPage(rid.pageNum, page);
    unsigned slotsNum = this->getSlotsNum(page);
    unsigned newFreespace = this->getPageFreespace(page) - insertDataLen;
    unsigned newSlotsNum = slotsNum;
    rid.slotNum = this->findAvaiSlot(page);
    if (rid.slotNum > 0) {
        if (rid.slotNum != slotsNum) {
            insertAdd = this->getSlotAdd(page, rid.slotNum);
            shiftLength = PAGE_SIZE - 4 - insertAdd - this->getPageFreespace(page) - slotsNum * 4;
            memmove(page + insertAdd + insertDataLen, page + insertAdd, shiftLength);
        } else {
            insertAdd = this->getSlotAdd(page, rid.slotNum - 1) +
                        this->getSlotLen(page, rid.slotNum - 1);
        }
    }
    memcpy(&page[insertAdd], catalog, CATALOGBYTE_SIZE);
    memcpy(&page[insertAdd + CATALOGBYTE_SIZE], data, dataLen);
    for (unsigned i = rid.slotNum+1; i < slotsNum; i++) {
        this->setSlotAdd(page, i, this->getSlotAdd(page, i) + insertDataLen);
    }
    if(rid.slotNum == slotsNum) { newFreespace -= 4; newSlotsNum ++; }
    this->updateSlotInfo(page, rid.slotNum, insertAdd, insertDataLen, newFreespace, newSlotsNum);
    fileHandle.writePage(rid.pageNum, page);
    fileHandle.updateIndexData(rid.pageNum, this->getPageFreespace(page));
    return STD_SUCC;
}

unsigned RecordBasedFileManager::findAvaiSlot(const void* data)
{
    int slotsNum = this->getSlotsNum(data);
    for (int slotIndex = 0; slotIndex < slotsNum; slotIndex++)
        if (this->getSlotLen(data, slotIndex) == 0) return slotIndex;
    return slotsNum;
}

void RecordBasedFileManager::updateSlotInfo(char* buf, const unsigned slotIndex,
    const unsigned insertAdd, const unsigned dataLen,
    const unsigned freespace, const unsigned totalSlotNums)
{
    this->setSlotAdd(buf, slotIndex, insertAdd); // directory address
    this->setSlotLen(buf, slotIndex, dataLen); // directory length
    this->setPageFreespace(buf, freespace) ; // F
    this->setSlotsNum(buf, totalSlotNums) ; // N
}

RC RecordBasedFileManager::getAttributeFromSlot(const vector<Attribute> &recordDescriptor, const string &attributeName, const char* slot, void *data)
{
    const int fieldNum = recordDescriptor.size();
    const int nullByteNum = (fieldNum-1)/8+1;
    const char *ptr = slot + nullByteNum;
    char view[PAGE_SIZE];
    for (int i = 0; i < fieldNum; i++)
    {
        memcpy(view, ptr, 200);
        if (*((byte*)slot+i/8) & (128 >> i%8))
        {
            if(recordDescriptor[i].name == attributeName)
            {
                *(char*)data |= 128;
                return STD_SUCC;
            }
            continue;
        }

        int curAttributeLen = 0;
        if (recordDescriptor[i].type == TypeInt) curAttributeLen = 4;
        else if (recordDescriptor[i].type == TypeReal) curAttributeLen = 4;
        else curAttributeLen = 4 + *((int*)ptr);
        if (recordDescriptor[i].name == attributeName)
        {
            memcpy((char*)data+1, ptr, curAttributeLen);
            return STD_SUCC;
        }
        else
            ptr += curAttributeLen;
    }
    return ATTRIBUTE_NOT_FOUND;
}

RC RecordBasedFileManager::getAttributesFromSlot(const vector<Attribute> &recordDescriptor, const vector<string> &attributeNames, const char* slot, void *data)
{
    // this->printRecord(recordDescriptor, data);
    if (attributeNames.size() == 0) return STD_SUCC;
    const int fieldNum = recordDescriptor.size();
    const int nullByteNum = (fieldNum-1)/8+1;
    const char *ptr = slot + nullByteNum;
    // add null byte for data.
    memset(data, 0, 1 + (attributeNames.size()-1)/8);
    char *t_data = (char*)data + 1 + (attributeNames.size()-1)/8;
    for (int j = 0; j < attributeNames.size(); j++)
    {
        const char *t_ptr = ptr;
        for (int i = 0; i < fieldNum; i++)
        {
            if (*((byte*)slot+i/8) & (128 >> i%8))
            {
                if(recordDescriptor[i].name == attributeNames[j])
                {
                    *((char*)data+j/8) |= (128 >> (j%8));
                }
                continue;
            }

            int curAttributeLen = 0;
            if (recordDescriptor[i].type == TypeInt) curAttributeLen = 4;
            else if (recordDescriptor[i].type == TypeReal) curAttributeLen = 4;
            else curAttributeLen = 4 + *((int*)t_ptr);
            if (recordDescriptor[i].name == attributeNames[j]) {
                // this->printOne(recordDescriptor[i], t_ptr);
                memcpy(t_data, t_ptr, curAttributeLen);
                t_data += curAttributeLen;
                break;
            }
            t_ptr += curAttributeLen;
        }
    }
    return STD_SUCC;
}

RC RecordBasedFileManager::shiftData(void* data, unsigned startOffset, unsigned newLength, unsigned originLength, unsigned shiftLength, unsigned numOfSlots)
{
    if (shiftLength == 0) return STD_SUCC;
    memmove((char *)data + startOffset + newLength, (char *)data + startOffset + originLength, shiftLength); // shift data backward
    // update address of records after the updated record
    for (int i = 0; i < numOfSlots; i++) {
        int address = this->getSlotAdd(data, i);
        if (address > startOffset) {
            this->setSlotAdd(data, i, address + newLength - originLength);
        }
    }
    return STD_SUCC;
}

RC RecordBasedFileManager::scan(FileHandle &fileHandle,
    const vector<Attribute> &recordDescriptor,
    const string &conditionAttribute,
    const CompOp compOp,                  // comparision type such as "<" and "="
    const void *value,                    // used in the comparison
    const vector<string> &attributeNames, // a list of projected attributes
    RBFM_ScanIterator &rbfm_ScanIterator)
{
    return rbfm_ScanIterator.setParams(fileHandle, recordDescriptor, conditionAttribute, compOp, value, attributeNames, this);
}


RC RecordBasedFileManager::scanNextRecord(RBFM_ScanIterator *rbfm_ScanIterator, RID &rid, void *data)
{
    if(!rbfm_ScanIterator->isOpen()) return SCAN_NOT_OPEN;
    rid = rbfm_ScanIterator->getRID();
    FileHandle *fileHandle;
    vector<Attribute> recordDescriptor;
    string conditionAttribute;
    CompOp compOp;
    const void *value;
    vector<string> attributeNames;
    AttrType attrType;
    rbfm_ScanIterator->getParams(&fileHandle, recordDescriptor, conditionAttribute,
        compOp, &value, attributeNames, attrType);

    char page[PAGE_SIZE];
    while(true)
    {
        if (rid.pageNum >= fileHandle->getNumberOfDataPages())
        {
            rbfm_ScanIterator->saveRID(rid);
            return RBFM_EOF;
        }
        RC rc = fileHandle->readPage(rid.pageNum, page);
        if(rc != STD_SUCC) { rbfm_ScanIterator->saveRID(rid); return rc; }
        unsigned slotsNum = this->getSlotsNum(page);
        char slot[PAGE_SIZE];
        while(rid.slotNum < slotsNum)
        {
            if (this->getSlotLen(page, rid.slotNum) == 0) { rid.slotNum++; continue;}
            if (this->isPointed(rid.slotNum, page)) continue;
            if (!this->isPointer(rid.slotNum, page))
            {
                memcpy(slot, &page[this->getSlotAdd(page, rid.slotNum) + CATALOGBYTE_SIZE], this->getSlotLen(page, rid.slotNum) - CATALOGBYTE_SIZE);
            }
            else
            {
                RID newRid;
                unsigned insertAdd = this->getSlotAdd(page, rid.slotNum);
                newRid.pageNum = *(unsigned*)(page + insertAdd + CATALOGBYTE_SIZE + SLOTBYTE_SIZE);
                newRid.slotNum = utils::getShortIntAt(page, insertAdd + CATALOGBYTE_SIZE);
                this->readRecord(*fileHandle, recordDescriptor, newRid, slot);
            }
            char attributes[PAGE_SIZE] = {};
            rc = this->getAttributeFromSlot(recordDescriptor, conditionAttribute, slot, attributes);
            if(rc != STD_SUCC && compOp != NO_OP) return rc;
            // judge if it satisfy condition
            if((attributes[0] & 128 && compOp != NO_OP))
            {
                rid.slotNum++;
                continue;
            }
            if (isSatisfyCondition(attributes+1, compOp, value, attrType))
            {
                rc = this->getAttributesFromSlot(recordDescriptor, attributeNames, slot, data);
                if(rc != STD_SUCC) return rc;
                RID newrid = rid;
                if(newrid.slotNum == slotsNum-1) {newrid.slotNum = 0; newrid.pageNum ++; }
                else {newrid.slotNum ++;}
                rbfm_ScanIterator->saveRID(newrid);
                return rc;
            }
            rid.slotNum ++;
        }
        rid.slotNum = 0;
        rid.pageNum ++;
    }
}

bool RecordBasedFileManager::isSatisfyCondition(const void* a, CompOp compOp, const void*b, AttrType &attrType)
{
    if(compOp == NO_OP) return true;
    if (attrType == TypeInt)
    {
        int ia = *((int*)(a)), ib = *((int*)(b));
        switch(compOp)
        {
            case EQ_OP:
            {return ia == ib;}
            case LT_OP:
            {return ia < ib;}
            case LE_OP:
            {return ia <= ib;}
            case GT_OP:
            {return ia > ib;}
            case GE_OP:
            {return ia >= ib;}
            case NE_OP:
            {return ia != ib;}
        }
    }
    else if(attrType == TypeReal)
    {
        float fa = *((float*)(a)), fb = *((float*)(b));
        switch(compOp)
        {
            case EQ_OP:
            {return fa == fb;}
            case LT_OP:
            {return fa < fb;}
            case LE_OP:
            {return fa <= fb;}
            case GT_OP:
            {return fa > fb;}
            case GE_OP:
            {return fa >= fb;}
            case NE_OP:
            {return fa != fb;}
        }
    }
    else if(attrType == TypeVarChar)
    {
        int lena = *((int*)a), lenb = *((int*)b);
        int minlen = lena > lenb ? lenb : lena;
        const char *_ta = 4+(char*)a, *_tb = 4+(char*)b;
        switch(compOp)
        {
            case EQ_OP:
            {return memcmp(a, b, 4+*((int*)b)) == 0;}
            case NE_OP:
            {return memcmp(a, b, 4+*((int*)b) != 0);}
            case LT_OP:
            {
                for(int i=0; i<lena && i<lenb; i++)
                {
                    if(*(_ta+i) < *(_tb+i)) return true;
                    if(*(_ta+i) > *(_tb+i)) return false;
                }
                if(lena < lenb) return true;
                else return false;
            }
            case LE_OP:
            {
                if(memcmp(a, b, 4+*((int*)b))) return true;
                for(int i=0; i<lena && i<lenb; i++)
                {
                    if(*(_ta+i) < *(_tb+i)) return true;
                    if(*(_ta+i) > *(_tb+i)) return false;
                }
                if(lena < lenb) return true;
                else return false;
            }
            case GT_OP:
            {
                for(int i=0; i<lena && i<lenb; i++)
                {
                    if(*(_ta+i) > *(_tb+i)) return true;
                    if(*(_ta+i) < *(_tb+i)) return false;
                }
                if(lena > lenb) return true;
                else return false;
            }
            case GE_OP:
            {
                if(memcmp(a, b, 4+*((int*)b))) return true;
                for(int i=0; i<lena && i<lenb; i++)
                {
                    if(*(_ta+i) > *(_tb+i)) return true;
                    if(*(_ta+i) < *(_tb+i)) return false;
                }
                if(lena > lenb) return true;
                else return false;
            }
        }
    }
}

RC RBFM_ScanIterator::setParams(FileHandle &fileHandle,
    const vector<Attribute> &recordDescriptor, const string &conditionAttribute,
    const CompOp compOp, const void *value, const vector<string> &attributeNames,
    RecordBasedFileManager *rbfm)
{
    this->_attrType = TypeInt;
    for (int i = 0; i < recordDescriptor.size(); i++ )
        if (recordDescriptor[i].name == conditionAttribute)
        {
            this->_attrType = recordDescriptor[i].type;
            break;
        }
    if(this->_isOpen) this->close();
    this->_fileHandle = &fileHandle;
    this->_recordDescriptor = recordDescriptor;
    this->_conditionAttribute = conditionAttribute;
    this->_compOp = compOp;
    this->_value = value;
    this->_attributeNames = attributeNames;
    this->_curRid.pageNum = 0;
    this->_curRid.slotNum = 0;
    this->_rbfm = rbfm;
    this->_isOpen = true;
    return STD_SUCC;
}

RC RBFM_ScanIterator::getParams(FileHandle **fileHandle,
    vector<Attribute> &recordDescriptor, string &conditionAttribute,
    CompOp &compOp, const void **value, vector<string> &attributeNames, AttrType& attr)
{
    if (!this->_isOpen) return SCAN_NOT_OPEN;
    *fileHandle = this->_fileHandle;
    recordDescriptor = this->_recordDescriptor;
    conditionAttribute = this->_conditionAttribute;
    compOp = this->_compOp;
    *value = this->_value;
    attributeNames = this->_attributeNames;
    attr = this->_attrType;
    return STD_SUCC;
}

void RBFM_ScanIterator::saveRID(RID rid)
{ this->_curRid = rid; }

RID RBFM_ScanIterator::getRID()
{ return this->_curRid; }

bool RBFM_ScanIterator::isOpen()
{ return this->_isOpen; }

RBFM_ScanIterator::RBFM_ScanIterator()
{
    this->_isOpen = false;
}

RBFM_ScanIterator::~RBFM_ScanIterator()
{
    if (this->_isOpen) this->close();
}

RC RBFM_ScanIterator::getNextRecord(RID &rid, void *data)
{
    if(!this->_isOpen) return SCAN_NOT_OPEN;
    return this->_rbfm->scanNextRecord(this, rid, data);
}

RC RBFM_ScanIterator::close()
{
    if (!this->_isOpen) return SCAN_NOT_OPEN;
    this->_isOpen = false;
    return STD_SUCC;
}

bool RecordBasedFileManager::isPointer(const unsigned slotNum, char *page)
{
    return (*(page+this->getSlotAdd(page, slotNum)) & 1 << 7);
}

bool RecordBasedFileManager::isPointed(const unsigned slotNum, char *page)
{
    return (*(page+this->getSlotAdd(page, slotNum)) & 1 << 6);
}
